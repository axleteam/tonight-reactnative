import { Toast } from 'native-base';
import { Linking } from 'react-native';
import imgShadow from '../../img/img_shadow.png';

export const checkError = (name, error) => {
  if (error === null || error === undefined) return false;
  return error[name];
};

export const dateFromIso = date => new Date(date).getDate();

export const monthFromIso = date => monthNames[new Date(date).getMonth()];

export const getTime = date => `${new Date(date).getHours()}:${new Date(date).getMinutes()}`;

const getYear = date => `${new Date(date).getFullYear()}`;

export const dateFormat = date => `${formatNumber(monthFromIso(date))}/${formatNumber(dateFromIso(date))}/01`;

export const dobFormat = date => `${dateFromIso(date)} ${monthFromIso(date)} ${getYear(date)}`;

export const checkImage = (img) => {
  let image = { uri: null };
  if (img === null || img === '') {
    image = imgShadow;
  } else {
    image.uri = img;
  }
  return image;
};
const formatNumber = number => (`0${number}`).slice(-2);

export const extract = (string, operator) => (string === null ? [] : string.split(operator));

export const alert = (message, type) => {
  Toast.show({
    text: message,
    buttonText: 'Okay',
    type,
    style: {
      backgroundColor: type === 'success' ? '#bf178e' : '#000',
    },
  });
};

export const openUrl = (url) => {
  Linking.openURL(url);
};

export const limitText = (text, limit) => (text !== null ? text.substring(0, limit) : '');
const monthNames = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
  'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек',
];

export const isoToDate = (isostring) => {
  const date = new Date(isostring);
  if (isostring) {
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    if (dt < 10) {
      dt = `0${dt}`;
    }
    if (month < 10) {
      month = `0${month}`;
    }

    return `${year}-${month}-${dt}`;
  }
  return undefined;
};

export const getDayMonth = (isostring) => {
  const date = new Date(isostring);
  const month = date.getMonth();
  const dt = date.getDate();
  return `${dt} ${monthNames[month]}`;
};

export const getPosition = (vertical, horizontal) => ({
  left: horizontal,
  top: vertical,
  bottom: vertical,
  right: horizontal,
});
