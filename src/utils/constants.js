export const INIT = 'INIT';
export const LOADING = 'LOADING';
export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';

export const UPTECH_SITE = 'https://uptech.team/';
export const API_URL = 'https://tonightwebapi20180422083559.azurewebsites.net/api/';
