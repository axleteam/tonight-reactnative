/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { PureComponent } from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { Root, Container } from 'native-base';
import TonightApp from './navigations';
import configureStore from './store/configureStore';
import NavigatorService from './utils/navigation';

export default class App extends PureComponent<{}> {
  render() {
    const store = configureStore();
    return (
      <Root>
        <Container>
          <StatusBar backgroundColor="rgba(0,0,0,0.5)" barStyle="light-content" />
          <Provider store={store}>
            <TonightApp ref={(navigatorRef) => {
          NavigatorService.setContainer(navigatorRef);
        }}
            />
          </Provider>
        </Container>
      </Root>
    );
  }
}

