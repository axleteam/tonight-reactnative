import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import MapView from 'react-native-maps';
import { Spinner } from 'native-base';
import Carousel from 'react-native-snap-carousel';
import geoIcon from '../../../img/geo_icon.png';
import BannerItem from '../BannerItem';
import { getEventRegion } from '../../store/event/duck';

const { height, width } = Dimensions.get('window');

class LocationMap extends React.Component {
  constructor() {
    super();
    this.carousel = {};
    this.state = {
      region: {
        latitude: 50.431782,
        longitude: 30.516382,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
  }

  getEvents = (region) => {
    const { isEvents, getEventsByRegion } = this.props;
    if (isEvents) {
      getEventsByRegion(region);
    }
  }

    renderItem = ({ item, index }) =>
      (
        <TouchableOpacity
          style={styles.imageContainer}
          activeOpacity={0.7}
          onPress={() => {
          this.carousel.snapToItem(index);
        }}
        >
          <BannerItem item={item} height={width / 3} width={width / 2} />
        </TouchableOpacity>
      );


    render() {
      const { events, phase } = this.props;
      const { region } = this.state;
      return (
        <View style={styles.mainContainer}>
          <MapView
            style={{ ...StyleSheet.absoluteFillObject }}
            initialRegion={region}
            onRegionChangeComplete={this.getEvents}
          >
            {events.map((event, index) => (
              <MapView.Marker
                coordinate={{
                  latitude: event.latitude,
                  longitude: event.longtitude,
            }}
                image={geoIcon}
                key={event.id}
                onPress={() => this.carousel.snapToItem(index)}
              />
          ))}
          </MapView>
          <View style={styles.sliderView}>
            {phase === 'LOADING' && <Spinner color="red" style={styles.loader} />}
            <Carousel
              ref={(c) => { this.carousel = c; }}
              data={events}
              renderItem={this.renderItem}
              sliderWidth={width}
              itemWidth={width / 2}
              layout="default"
              firstItem={1}
            />
          </View>
        </View>
      );
    }
}

const styles = StyleSheet.create({
  mainContainer: {
    ...StyleSheet.absoluteFillObject,
  },
  imageContainer: {
    width: width / 2,
  },
  sliderView: {
    position: 'absolute',
    zIndex: 1,
    top: height / 1.8,
  },
  loader: {
    alignItems: 'flex-start',
    paddingLeft: 20,
    position: 'absolute',
    bottom: 0,
    zIndex: 99999,
  },
});

LocationMap.propTypes = {
  getEventsByRegion: PropTypes.func,
  isEvents: PropTypes.bool,
  events: PropTypes.array,
  phase: PropTypes.string,
};

const mapStateToProps = ({ eventsRegionStore }) => eventsRegionStore;

const mapDispatchToProps = {
  getEventsByRegion: region => getEventRegion(region),
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationMap);
