import React from 'react';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { getPosition } from '../../utils/methods';

const { width } = Dimensions.get('window');
LocaleConfig.locales.ru = {
  monthNames: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
  monthNamesShort: ['Ян', 'Фе', 'Ма', 'Ап', 'Ма', 'Ию', 'Ию', 'Ав', 'Се', 'Ок', 'Но', 'Де'],
  dayNames: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
  dayNamesShort: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
};

LocaleConfig.defaultLocale = 'ru';
const TonightCalendar = ({ onHide, onPressDay, selectedDate }) =>
  (
    <View style={styles.modalStyle}>
      <View style={styles.calendarStyle}>
        <TouchableOpacity
          onPress={onHide}
          style={{ marginVertical: 10, paddingRight: 10, alignSelf: 'flex-end' }}
          hitSlop={getPosition(20, 10)}
        >
          <Icon name="md-close" size={25} color="#19094f" />
        </TouchableOpacity>
        <Calendar
          markedDates={selectedDate ? { [selectedDate]: { selected: true } } : {}}
          style={{ height: width - 60 }}
          theme={{
                textSectionTitleColor: '#19094f',
                selectedDayBackgroundColor: '#ff00a8',
                monthTextColor: '#19094f',
                todayTextColor: '#ff00a8',
                arrowColor: '#19094f',
              }}
          onDayPress={onPressDay}
        />
      </View>
    </View>
  );

const styles = {
  modalStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  calendarStyle: {
    width: width - 40,
    height: width,
    backgroundColor: '#fff',
    borderRadius: 10,
    marginVertical: 10,
  },
};

TonightCalendar.propTypes = {
  onHide: PropTypes.func,
  onPressDay: PropTypes.func,
  selectedDate: PropTypes.any,
};

export default TonightCalendar;
