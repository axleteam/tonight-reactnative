import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Text, View, CheckBox } from 'native-base';

const TonightCheckbox = ({
  label, checked,
}) => (
  <View style={styles.viewCheckbox}>
    <CheckBox checked={checked} color="#a1047d" style={styles.checkboxStyle} />
    <Text style={styles.checkboxTextStyle}>{label}</Text>
  </View>
);

TonightCheckbox.propTypes = {
  label: PropTypes.string,
  checked: PropTypes.bool,
};

const styles = StyleSheet.create({

  checkboxTextStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
  },
  viewCheckbox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
  },
  checkboxStyle: {
    borderRadius: 5,
    marginRight: 15,
    alignSelf: 'center',
  },
});

export default TonightCheckbox;
