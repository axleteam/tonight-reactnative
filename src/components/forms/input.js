import React from 'react';
import { Item, Input, Icon, View, Text } from 'native-base';
import PropTypes from 'prop-types';
import globalStyles from '../../assets/globalcss';

const {
  itemStyle, inputMargin, errorStyle,
} = globalStyles;

const TonightInput = ({
  placeholder,
  onChange,
  name,
  secret,
  error,
  backgroundColor,
  color,
  value,
}) => (
  <View>
    <Item inlineLabel last style={[{ backgroundColor }, itemStyle, inputMargin]}>
      <Input
        placeholder={placeholder}
        style={{ color }}
        placeholderTextColor="#C7C6E6"
        onChangeText={onChange.bind(this, name)}
        secureTextEntry={secret}
        autoCapitalize="none"
        value={value}
      />
      {error ? <Icon name="close-circle" /> : null}
    </Item>
    {error ? <Text style={errorStyle}>{error}</Text> : null}
  </View>
);

TonightInput.defaultProps = {
  backgroundColor: '#9382B1',
  color: '#fff',
  secret: false,
  error: undefined,
  value: '',
};

TonightInput.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
  secret: PropTypes.bool,
  error: PropTypes.any,
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  value: PropTypes.string,
};

export default TonightInput;
