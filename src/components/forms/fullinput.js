import React from 'react';
import PropTypes from 'prop-types';
import { Item, Input, Icon, View, Text, Label } from 'native-base';
import globalStyles from '../../assets/globalcss';

const {
  errorStyle, labelStyle,
} = globalStyles;

const TonightFullInput = ({
  placeholder,
  onChange,
  name,
  error,
  backgroundColor,
  value,
  editable,
  onTouchStart,
}) => (
  <View>
    <Item fixedLabel style={{ backgroundColor }} onPress={onTouchStart.bind(this)}>
      <Label style={labelStyle}>{placeholder}</Label>
      <Input
        value={value}
        placeholder={placeholder}
        style={{ color: '#19094f', flex: 2.5 }}
        onChangeText={onChange.bind(this, name)}
        autoCapitalize="none"
        editable={editable}
      />
      {error ? <Icon name="close-circle" /> : null}
    </Item>
    {error ? <Text style={errorStyle}>{error}</Text> : null}
  </View>
);

TonightFullInput.defaultProps = {
  backgroundColor: '#fff',
  error: undefined,
  editable: true,
  onTouchStart: () => null,
};

TonightFullInput.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
  backgroundColor: PropTypes.string,
  error: PropTypes.string,
  value: PropTypes.string,
  editable: PropTypes.bool,
  onTouchStart: PropTypes.func,
};

export default TonightFullInput;
