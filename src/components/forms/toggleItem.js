import React from 'react';
import PropTypes from 'prop-types';
import { ListItem, Text, Body, Right, Switch, View } from 'native-base';

const ToggleItem = ({
  marginTop, label, value, text,
}) => (
  <View>
    <ListItem noIndent style={{ backgroundColor: '#fff', marginTop }}>
      <Body>
        <Text style={{ color: '#BFBFBF', fontFamily: 'HelveticaNeueCyr-Roman' }} >{label}</Text>
      </Body>
      <Right>
        <Switch
          value={value}
          tintColor="#a1047d"
          onTintColor={value ? '#a1047d' : '#fff'}
          thumbTintColor={value ? '#fff' : '#a1047d'}
        />
      </Right>
    </ListItem>
    {text !== undefined &&
    <View style={{ paddingLeft: 25, marginTop: 10 }}>
      <Text style={{ color: '#80ffffff', fontSize: 14 }}>{text}</Text>
    </View>
    }

  </View>
);

ToggleItem.propTypes = {
  label: PropTypes.string,
  value: PropTypes.bool,
  text: PropTypes.string,
  marginTop: PropTypes.number,
};

ToggleItem.defaultProps = {
  marginTop: 20,
};

export default ToggleItem;
