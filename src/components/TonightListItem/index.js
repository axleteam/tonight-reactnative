import React from 'react';
import PropTypes from 'prop-types';
import { ListItem, Left, Text, Right, Body } from 'native-base';
import { Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const TonightListItem = ({
  name, image, size, onPress,
}) => (
  <ListItem icon style={{ height: 55 }}>
    <Left>
      <Image source={image} style={{ width: size, height: size }} resizeMode="contain" />
    </Left>
    <Body>
      <TouchableOpacity onPress={onPress}>
        <Text style={{ color: '#fff', fontWeight: 'bold' }}>{ name }</Text>
      </TouchableOpacity>
    </Body>
    <Right style={{ alignSelf: 'center' }}>
      <Icon name="chevron-right" color="#fff" size={size} />
    </Right>
  </ListItem>
);

TonightListItem.propTypes = {
  name: PropTypes.string,
  image: PropTypes.any,
  size: PropTypes.number,
  onPress: PropTypes.func,
};

export default TonightListItem;
