import React from 'react';
import { ScrollView, Dimensions, View } from 'react-native';
import PropTypes from 'prop-types';
import BannerItem from '../../components/BannerItem';

const { width } = Dimensions.get('window');

const ImageScroll = ({ events, style }) =>
  (
    <ScrollView horizontal {...style} showsHorizontalScrollIndicator={false}>
      {events.map(event =>
        (
          <View key={event.uid} style={styles.bannerPadding}>
            <BannerItem item={event} height={width / 2.2} width={width / 1.4} />
          </View>
    ))}
    </ScrollView>
  );

ImageScroll.propTypes = {
  events: PropTypes.array,
  style: PropTypes.object,
};

const styles = {
  bannerPadding: {
    paddingRight: 15,
  },
};

export default ImageScroll;
