import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import PlaceGrid from '../PlaceGrid';
import LocationMap from '../LocationMap';

class LocationTab extends PureComponent {
  render() {
    const { location } = this.props;
    return (
      <View style={{ flex: 1 }}>
        {location ?
          <LocationMap />
            :
          <PlaceGrid />
        }
      </View>);
  }
}


LocationTab.propTypes = {
  location: PropTypes.bool,
};

export default LocationTab;
