import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import EventTab from './EventTab';
import LocationTab from './LocationTab';

class TabData extends Component {
  renderData = (type) => {
    const { navigation, location } = this.props;

    if (type === 'Event') {
      return (
        <EventTab navigation={navigation} location={location} />
      );
    } else if (type === 'Location') {
      return (
        <LocationTab navigation={navigation} location={location} />
      );
    }
    return null;
  };
  render() {
    const { heading } = this.props;
    return (
      <View style={styles.scrollView}>
        {this.renderData(heading)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#ECECEC',
    ...StyleSheet.absoluteFillObject,
  },
});

TabData.propTypes = {
  navigation: PropTypes.object,
  heading: PropTypes.string,
  location: PropTypes.bool,
};

export default TabData;
