import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Spinner } from 'native-base';
import { ScrollView, View, Dimensions, StyleSheet } from 'react-native';
import { alert } from '../../utils/methods';
import globalStyles from '../../assets/globalcss';
import { addFavouriteEvent, removeFavouriteEvent } from '../../store/favourite/duck';
import NotFound from '../NotFound';
import BannerItem from '../BannerItem';
import LocationMap from '../LocationMap';

const { height } = Dimensions.get('window');

class EventTab extends Component {
  addFavourite = ({ id }) => {
    const events = this.toggleFavourite(true, id);
    alert('Added to Favourite', 'success');
    this.props.addFavouriteEvent({ data: { eventId: id }, events });
  }

  removeFavourite= ({ id }) => {
    const events = this.toggleFavourite(false, id);
    alert('Removed from Favourite');
    this.props.removeFavouriteEvent({ data: { eventId: id }, events });
  }

  toggleFavourite = (value, id) => {
    const { events } = this.props.eventsStore;
    const index = events.findIndex(event => event.id === id);
    events[index].isPlannedForUser = value;
    return events;
  }

  gotoEventDetail = id => this.props.navigation.navigate('EventDetail', { id });

  render() {
    const { events, phase } = this.props.eventsStore;
    const { location } = this.props;
    return (
      <View style={{ ...StyleSheet.absoluteFillObject }}>
        {location ?
          <LocationMap isEvents />
          :
          <View>
            {phase === 'LOADING' && <Spinner color="red" style={globalStyles.loader} />}
            <ScrollView showsVerticalScrollIndicator={false}>
              {events.map((event, index) =>
            (
              <View key={event.id}>
                <BannerItem
                  item={event}
                  width="100%"
                  height={height / 4}
                  tags
                  favourite
                  onAddFavourite={item => this.addFavourite(item)}
                  onRemoveFavourite={item => this.removeFavourite(item)}
                  index={index}
                />
              </View>
          ))}
              {phase !== 'LOADING' && events.length === 0 ?
                <NotFound type="Event" />
        :
        null}
            </ScrollView>
          </View>
      }
      </View>
    );
  }
}

EventTab.propTypes = {
  addFavouriteEvent: PropTypes.func,
  removeFavouriteEvent: PropTypes.func,
  eventsStore: PropTypes.object,
  navigation: PropTypes.object,
  location: PropTypes.bool,
};


const mapStateToProps = ({ eventsStore, favouriteEventStore }) => (
  { eventsStore, favouriteEventStore }
);

const mapDispatchToProps = {
  addFavouriteEvent: data => addFavouriteEvent(data),
  removeFavouriteEvent: data => removeFavouriteEvent(data),
};


export default connect(mapStateToProps, mapDispatchToProps)(EventTab);
