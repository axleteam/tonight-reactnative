import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class ListItem extends React.PureComponent {
    onPress = () => {
      this.props.onPressItem(this.props.id);
    };

    render() {
      const textColor = this.props.selected ? '#c52e99' : '#fff';
      const iconColor = this.props.selected ? '#c52e99' : 'transparent';
      const {
        iconStyle,
      } = styles;
      return (
        <TouchableOpacity onPress={this.onPress} style={{ marginTop: 8, alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>
            <Icon style={iconStyle} name="md-checkmark" size={20} color={iconColor} />
            <Text style={{ color: textColor, fontSize: 20 }}>
              {this.props.title}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
}

const styles = StyleSheet.create({
  iconStyle: {
    marginRight: 5, alignSelf: 'center',
  },
});

ListItem.propTypes = {
  title: PropTypes.string,
  onPressItem: PropTypes.func,
  selected: PropTypes.bool,
  id: PropTypes.string,
};

export default ListItem;
