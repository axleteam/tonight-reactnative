import React from 'react';
import { Image, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { checkImage, limitText, monthFromIso, dateFromIso } from '../../utils/methods';
import globalStyles from '../../assets/globalcss';
import NavigatorService from '../../utils/navigation';

const imgShadow = require('../../../img/img_shadow.png');
const imgShadowBlack = require('../../../img/img_shadow_black.png');

const BannerItem = ({
  item,
  height,
  width,
  tags,
  onAddFavourite,
  onRemoveFavourite,
  favourite,
  index,
  date,
  place,
  shadow,
}) => (
  <TouchableOpacity
    onPress={() => NavigatorService.navigate(place ? 'PlaceDetail' : 'EventDetail', { id: item.id === 0 ? item.uid : item.id })}
    key={item.id}
    activeOpacity={0.7}
  >

    <Image source={shadow || (index % 2 ? imgShadowBlack : imgShadow)} style={[styles.imgStyle, styles.imgShadowStyle, { height: height / 2 }]} resizeMode="stretch" />
    <Image
      style={{ height, width }}
      source={checkImage(item.imagePath)}
    />
    {favourite &&
    <View style={styles.favView}>
      <TouchableOpacity
        onPress={item.isPlannedForUser ?
          onRemoveFavourite.bind(this, item) :
          onAddFavourite.bind(this, item)}
      >
        <Icon
          name={item.isPlannedForUser ? 'heart' : 'heart-outline'}
          color={item.isPlannedForUser ? '#bf178e' : '#fff'}
          size={25}
        />
      </TouchableOpacity>
    </View>
    }
    <View style={styles.imgBottom}>
      {date &&
      <View style={styles.headingView}>
        <Text style={styles.textStyle}>{dateFromIso(item.startTime || item.start_time)}</Text>
        <Text style={styles.textStyle}>{monthFromIso(item.startTime || item.start_time)}</Text>
      </View>
    }

      <View style={styles.textView}>
        <Text style={styles.eventTitleHeading}>{limitText(item.name, place ? 10 : 20)}</Text>
        <Text style={styles.authorHeading}>
          { item.place !== undefined &&
          limitText(typeof item.place === 'object' ? item.place.name : item.place, 15)
          }
          { item.location !== undefined &&
          limitText(typeof item.location === 'object' ? item.location.name : item.location, 15)
          }
        </Text>
      </View>
      {tags &&
      <View style={[globalStyles.labelView, styles.tagsPosition]}>
        {item.tags && item.tags.map((tag, i) =>
           (
             <View style={globalStyles.label} key={i}>
               <Text style={globalStyles.labelColor}>{tag}</Text>
             </View>
          ))}
      </View>
                }
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  imgBottom: {
    position: 'absolute',
    zIndex: 999,
    bottom: 15,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
  },
  headingView: {
    marginLeft: 15,
    width: 40,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    alignItems: 'center',
    borderRadius: 10,
    padding: 5,
  },
  textStyle: {
    fontSize: 12,
    fontWeight: '900',
  },
  textView: {
    backgroundColor: 'transparent',
    marginLeft: 12,
  },
  eventTitleHeading: {
    color: '#fff',
    fontFamily: 'HelveticaNeue-Bold',
    fontSize: 16,
    fontWeight: 'bold',
  },
  authorHeading: {
    color: '#fff',
    fontFamily: 'HelveticaNeue-Bold',
    fontSize: 12,
    fontWeight: 'bold',
  },
  tagsPosition: {
    top: 15,
  },
  favView: {
    position: 'absolute',
    backgroundColor: 'transparent',
    right: 10,
    top: 10,
  },
  imgShadowStyle: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    zIndex: 111,
  },
});

BannerItem.propTypes = {
  item: PropTypes.object,
  height: PropTypes.number,
  width: PropTypes.any,
  tags: PropTypes.bool,
  favourite: PropTypes.bool,
  onAddFavourite: PropTypes.func,
  onRemoveFavourite: PropTypes.func,
  index: PropTypes.number,
  date: PropTypes.bool,
  place: PropTypes.bool,
  shadow: PropTypes.any,
};

BannerItem.defaultProps = {
  tags: false,
  favourite: false,
  index: 0,
  date: true,
  place: false,
};

export default BannerItem;
