import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getEvents } from '../../store/event/duck';

const NotFound = ({ type, getDefaultEvents }) =>
  (
    <View style={styles.viewStyle}>
      <Text style={styles.textStyle}>
        Упс! Мы не нашил никаких результатов c выбранными фильтрами.
        Попробуй выбрать другие. Уверены все получится
      </Text>
      <TouchableOpacity onPress={getData.bind(this, { type, getDefaultEvents })}>
        <Text>Очистить Фильтры</Text>
      </TouchableOpacity>
    </View>
  );

const getData = ({ type, getDefaultEvents }) => {
  switch (type) {
    case 'Event':
      getDefaultEvents();
      break;
    default:
      break;
  }
};

const styles = {
  viewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 40,
    marginTop: 40,
  },
  textStyle: {
    fontSize: 20,
    textAlign: 'center',
    color: '#bf178e',
  },
};

NotFound.propTypes = {
  type: PropTypes.any,
  getDefaultEvents: PropTypes.func,
};

NotFound.defaultProps = {
  type: null,
};

const mapStateToProps = eventsStore => eventsStore;

const mapDispatchToProps = {
  getDefaultEvents: () => getEvents({}),
};

export default connect(mapStateToProps, mapDispatchToProps)(NotFound);

