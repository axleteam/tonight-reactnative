import React from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { View, StyleSheet } from 'react-native';

const Map = ({ lat, long, style }) =>
  (
    <View style={style}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={{ ...StyleSheet.absoluteFillObject }}
        initialRegion={{
                latitude: lat === null || lat === undefined ? 0 : lat,
                longitude: long === null || long === undefined ? 0 : long,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }}
        minZoomLevel={12}
        scrollEnabled={false}
      >
        <MapView.Marker
          coordinate={{
            latitude: lat === null || lat === undefined ? 0 : lat,
            longitude: long === null || long === undefined ? 0 : long,
          }}
        />
      </MapView>
    </View>
  );

export default Map;

