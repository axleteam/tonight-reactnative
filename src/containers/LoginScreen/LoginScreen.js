import React, { Component } from 'react';
import { View, ImageBackground, Text, TouchableOpacity } from 'react-native';
import { Button, Spinner, Title, Body } from 'native-base';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import SafeView from 'react-native-safe-area-view';
import TonightInput from '../../components/forms/input';
import styles from './style';
import globalStyles from '../../assets/globalcss';
import { checkError, getPosition } from '../../utils/methods';

const bgImage = require('../../../img/login_bg.png');

class LoginScreen extends Component {
  static navigationOptions = () => ({
    header: null,
    gesturesEnabled: false,
  });

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      formValues: {},
    };
  }
  componentWillReceiveProps = (nextProps) => {
    if (nextProps.loginStore.phase === 'SUCCESS') {
      this.props.navigation.navigate('Events');
    }
  }

  handlerClick = () => {
    this.props.navigation.navigate('ForgotPassword');
  };

  handleSubmit = () => {
    const { formValues } = this.state;
    this.props.userLogin(formValues);
  };

  handleChange = (field, value) => {
    const formValues = { ...this.state.formValues };
    formValues[field] = value;
    this.setState(() => ({ formValues }));
  };

  render() {
    const {
      container,
      bgImageContainer,
      bottomView,
      buttonStyle,
      buttonFont,
      bottomLink,
      bottomText,
      headerStyle,
      leftButton,
      centerTitle,
      rightButton,
      rightText,
    } = styles;

    const { errorStyle, loader, safeViewStyle } = globalStyles;
    const { loginStore, navigation } = this.props;
    const { errors, phase } = loginStore;
    const { formValues } = this.state;
    return (
      <SafeView style={safeViewStyle}>
        <ImageBackground style={bgImageContainer} source={bgImage}>
          {phase === 'LOADING' ? <Spinner color="red" style={loader} /> : null}
          <View style={headerStyle}>
            <Button
              transparent
              hitSlop={getPosition(20, 20)}
              onPress={() => navigation.navigate('Home')}
            >
              <Icon name="ios-arrow-back" color="#fff" size={20} />
            </Button>
            <Body><Title style={centerTitle}>Login</Title></Body>
            <View style={rightButton} />
          </View>
          <View style={{ opacity: phase === 'LOADING' ? 0.5 : 1 }}>
            <View style={{ paddingTop: 20 }}>
              <TonightInput
                value={formValues.username || ''}
                placeholder="Эл. адрес"
                onChange={this.handleChange}
                name="username"
                error={checkError('UserName', errors)}
              />
              <TonightInput
                value={formValues.password || ''}
                placeholder="Пароль"
                onChange={this.handleChange}
                name="password"
                secret
                error={checkError('Password', errors)}
              />
              {errors !== null && typeof errors.login_failure !== 'undefined' ? (
                <Text style={errorStyle}>{errors.login_failure}</Text>
            ) : null}
            </View>
            <View style={bottomView}>
              <Button style={buttonStyle} onPress={this.handleSubmit} disabled={phase === 'LOADING'}>
                <Text style={buttonFont}>Вход</Text>
              </Button>
              <TouchableOpacity onPress={this.handlerClick}>
                <Text style={bottomLink}>
                Забыли пароль? <Text style={bottomText}>Нажмите здесь</Text>
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </SafeView>
    );
  }
}

LoginScreen.propTypes = {
  userLogin: PropTypes.func,
  loginStore: PropTypes.object,
  navigation: PropTypes.object,
};

export default LoginScreen;
