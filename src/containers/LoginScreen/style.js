import { StyleSheet, Dimensions, Platform } from 'react-native';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  headerStyle: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  leftButton: {
    width: '20%',
  },
  centerTitle: {
    color: '#fff',
    fontSize: 20,
  },
  bgImageContainer: {
    flex: 1,
    width: '100%',
  },
  itemStyle: {
    backgroundColor: '#9382B1',
    height: 50,
    borderBottomWidth: 0,
  },
  inputStyle: {
    color: '#fff',
  },
  bottomView: {
    width,
    alignItems: 'center',
  },
  buttonStyle: {
    width: '85%',
    justifyContent: 'center',
    marginTop: 20,
    backgroundColor: '#A7007A',
    borderRadius: 10,
    alignSelf: 'center',
  },
  buttonFont: {
    color: '#fff',
    fontSize: Platform.OS === 'ios' ? 16 : 14,
    fontWeight: '800',
  },
  bottomLink: {
    color: '#fff',
    fontSize: 16,
    marginTop: 10,
  },
  bottomText: {
    color: '#A7007A',
  },
  titleStyle: {
    color: '#fff',
    fontSize: 18,
  },
  centerContent: {
    justifyContent: 'center',
  },

});

export default styles;
