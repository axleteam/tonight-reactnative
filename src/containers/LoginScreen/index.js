import { connect } from 'react-redux';
import LoginScreen from './LoginScreen';
import { loginUser } from '../../store/auth/duck';

const mapStateToProps = ({ loginStore }) => ({
  loginStore,
});

const mapDispatchToProps = {
  userLogin: cred => loginUser(cred),
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
