import { connect } from 'react-redux';
import Support from './Support';
import { addSupportMessage } from '../../store/support/duck';

const mapStateToProps = ({ supportStore }) => supportStore;

const mapDispatchToProps = {
  addSupportMessage: message => addSupportMessage(message),
};
export default connect(mapStateToProps, mapDispatchToProps)(Support);
