import React, { PureComponent } from 'react';
import { Text, StyleSheet, ImageBackground, View, Image, Dimensions, Platform, Keyboard } from 'react-native';
import SafeView from 'react-native-safe-area-view';
import PropTypes from 'prop-types';
import { List, Button, Spinner } from 'native-base';
import Communications from 'react-native-communications';
import globalStyles from '../../assets/globalcss';
import bgImage from '../../../img/login_bg.png';
import TonightInput from '../../components/forms/input';
import TonightListItem from '../../components/TonightListItem';
import phoneIcon from '../../../img/phone_icon.png';
import mainIcon from '../../../img/mail_icon.png';
import imgShadow from '../../../img/img_shadow.png';

const { height } = Dimensions.get('window');
class Support extends PureComponent {
    static navigationOptions = () => ({
      title: 'Поддержка',
      headerTintColor: '#fff',
      headerStyle: globalStyles.headerStyle,
      headerTitleStyle: globalStyles.headerTitleStyle,
      headerTransparent: true,
    });

    state={
      question: '',
    }

    componentWillReceiveProps= ({ phase }) => {
      if (phase === 'SUCCESS') {
        alert('Sent Message Successfully');
        this.setState(() => ({ question: '' }));
      }
    }

    getMessage = (field, value) => {
      this.setState(() => ({ question: value }));
    };

    sendMessage = () => {
      Keyboard.dismiss();
      this.props.addSupportMessage(this.state);
    }

    render() {
      const { safeViewStyle } = globalStyles;
      const {
        bgImageContainer,
        titleStyle,
        titleContainer,
        titleHead,
        titleTextStyle,
        buttonContainer,
        buttonStyle,
        buttonText,
      } = styles;
      const { loader } = globalStyles;
      const { phase } = this.props;
      return (
        <SafeView style={[safeViewStyle, { opacity: phase === 'LOADING' ? 0.6 : 1 }]}>
          <ImageBackground style={bgImageContainer} source={bgImage}>
            {phase === 'LOADING' ? <Spinner color="red" style={loader} /> : null}
            <View style={titleContainer}>
              <View>
                <Text style={titleStyle}>Как мы можем вам помочь?</Text>
              </View>
              <View style={{ paddingTop: 20 }}>
                <TonightInput
                  value={this.state.question}
                  placeholder="Напишите свой вопрос"
                  onChange={this.getMessage}
                  name="message"
                  backgroundColor="#fff"
                  color="#000"
                />
              </View>
              <View style={buttonContainer}>
                <Button
                  rounded
                  style={[buttonStyle, { backgroundColor: '#a1047d' }]}
                  onPress={this.sendMessage}
                  disabled={phase === 'LOADING'}
                >
                  <Text style={buttonText}>Отправить</Text>
                </Button>
              </View>
            </View>
            <View style={titleHead}>
              <Text style={titleTextStyle}>Поддержка</Text>
            </View>
            <List style={bgImageContainer}>
              <TonightListItem name="8 (800) 804-63-02" image={phoneIcon} size={20} onPress={() => Communications.phonecall('8 (800) 804-63-02', true)} />
              <TonightListItem name="say.hello@tonight.com" image={mainIcon} size={20} onPress={() => Communications.email('say.hello@tonight.com', null, null, 'My Subject', 'My body text')} />
            </List>
            <View style={titleHead}>
              <Text style={titleTextStyle}>Написать разработчикам</Text>
            </View>
            <List style={bgImageContainer}>
              <TonightListItem name="say.hello@tonight.com" image={mainIcon} size={20} onPress={() => Communications.email('say.hello@tonight.com', null, null, 'My Subject', 'My body text')} />
            </List>
            <Image source={imgShadow} style={{ flex: 1, height: height / 2, width: '100%' }} resizeMode="stretch" />
          </ImageBackground>
        </SafeView>
      );
    }
}

const styles = StyleSheet.create({
  bgImageContainer: {
    flex: 1,
    width: '100%',
  },
  logoContainer: {
    marginVertical: 55,
    alignItems: 'center',
  },
  imageLogo: {
    marginTop: 10,
  },
  titleContainer: {
    paddingTop: 60,
    backgroundColor: 'transparent',
  },
  titleStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'HelveticaNeueCyr-Roman',

  },
  titleHead: {
    backgroundColor: 'transparent',
    paddingLeft: 12,
    paddingTop: 20,
  },
  titleTextStyle: {
    color: '#fff',
    fontSize: 16,
  },
  buttonContainer: {
    paddingTop: 10,
    paddingHorizontal: 20,
  },
  buttonStyle: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    flex: Platform.OS === 'ios' ? 1 : 0,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 5,
  },
});

Support.propTypes = {
  addSupportMessage: PropTypes.func,
  phase: PropTypes.string,
};

export default Support;
