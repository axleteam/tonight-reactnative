import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  Image,
  ImageBackground,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import SafeView from 'react-native-safe-area-view';
import Icon from 'react-native-vector-icons/Ionicons';
import { Button, ActionSheet, Title, Spinner } from 'native-base';
import PropTypes from 'prop-types';
import ImagePicker from 'react-native-image-crop-picker';
import { getPosition, dobFormat, checkImage, alert } from '../../utils/methods';
import globalStyles from '../../assets/globalcss';
import bgImage from '../../../img/login_bg.png';
import TonightFullInput from '../../components/forms/fullinput';

const { height } = Dimensions.get('window');
const cameraButtons = ['Camera', 'Gallery', 'Cancel'];

class EditProfile extends Component {
  static navigationOptions = () => ({
    header: null,
    gesturesEnabled: false,
  });

    state = {
      uploadedFile: null,
      user: {},
      isDateTimePickerVisible: false,
    }

    componentDidMount = () => {
      if (this.props.user !== this.state.user) {
        const { user } = this.props;
        this.setState(() => ({ user }));
      }
      this.props.getUser();
    }

    componentWillReceiveProps = (nextProps) => {
      if (nextProps.editphase === 'SUCCESS') {
        alert('Edit Profile Successfully', 'success');
      }
    }

    componentDidUpdate = (prevProps) => {
      if (this.props.user !== prevProps.user) {
        const { user } = this.props;
        this.setState(() => ({ user }));
      }
    }


    handleChange = (field, value) => {
      const user = { ...this.state.user };
      user[field] = value;
      this.setState(() => ({ user }));
    };

    openActionCamera = () => {
      ActionSheet.show(
        {
          options: cameraButtons,
          cancelButtonIndex: 2,
          title: 'Select Option',
        },
        (buttonIndex) => {
          switch (buttonIndex) {
            case 0:
              this.openCamera();
              break;
            case 1:
              this.openGallery();
              break;
            default:
              break;
          }
        },
      );
    }

    openCamera = () => {
      ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      }).then((image) => {
        this.setState(() => ({
          uploadedFile: {
            uri: image.path,
            type: image.mime,
            name: new Date().toDateString(),
          },
        }));
      });
    }

    openGallery = () => {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
      }).then((image) => {
        this.setState(() => ({
          uploadedFile: {
            uri: image.path,
            type: image.mime,
            name: new Date().toDateString(),
          },
        }));
      });
    }

    updateProfile = () => {
      const { user, uploadedFile } = this.state;
      if (uploadedFile !== null) {
        user.uploadedFile = uploadedFile;
      }
      this.props.updateProfile(user);
    }

    handleDatePicked = (date) => {
      const user = { ...this.state.user };
      user.dateOfBirthday = new Date(date).toISOString();
      this.setState(() => ({ user }));
      this.togglePicker();
    };

    togglePicker = () => {
      this.setState(() => ({ isDateTimePickerVisible: !this.state.isDateTimePickerVisible }));
    }


    render() {
      const { safeViewStyle, loader } = globalStyles;
      const {
        bgImageContainer,
        cameraViewContainer,
        imageStyle,
        headerStyle,
        leftButton,
        rightButton,
        centerTitle,
        rightText,
        topContainer,
      } = styles;
      const { uploadedFile, user } = this.state;
      const { navigation, editphase } = this.props;
      return (
        <SafeView style={safeViewStyle}>
          <ImageBackground style={[bgImageContainer, { opacity: editphase === 'LOADING' ? 0.6 : 1 }]} source={bgImage}>
            {editphase === 'LOADING' ? <Spinner color="red" style={loader} /> : null}
            <View style={headerStyle}>
              <Button
                transparent
                hitSlop={getPosition(20, 10)}
                onPress={() => navigation.goBack()}
                style={leftButton}
              >
                <Icon name="ios-arrow-back" color="#fff" size={14} />
              </Button>
              <Title style={centerTitle}>Редактировать профиль</Title>
              <Button
                style={rightButton}
                transparent
                hitSlop={getPosition(20, 10)}
                onPress={this.updateProfile}
              >
                <Text style={rightText}>Готово</Text>
              </Button>
            </View>
            <ScrollView style={{ backgroundColor: 'transparent' }}>
              <View style={topContainer}>
                <View>
                  <Image
                    source={uploadedFile || checkImage(user.pictureUrl)}
                    style={imageStyle}
                  />
                  <View style={cameraViewContainer}>
                    <TouchableOpacity onPress={this.openActionCamera}>
                      <Icon name="ios-camera" size={30} color="#fff" />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={{ paddingBottom: 40 }}>
                <TonightFullInput
                  placeholder="Имя"
                  onChange={this.handleChange}
                  value={user.firstName || ''}
                  name="firstName"
                />
                <TonightFullInput
                  placeholder="Фамилия"
                  onChange={this.handleChange}
                  value={user.lastName || ''}
                  name="lastName"
                />
                <TonightFullInput
                  placeholder="E-mail"
                  onChange={this.handleChange}
                  value={user.email || ''}
                  name="email"
                  editable={false}
                />
              </View>
              <View style={{ paddingBottom: 40 }}>
                <TonightFullInput
                  placeholder="Фильтры"
                  onChange={this.handleChange}
                />
              </View>
              <View style={{ paddingBottom: 40 }}>
                <TonightFullInput
                  placeholder="Дата рождения"
                  onChange={this.handleChange}
                  onTouchStart={this.togglePicker}
                  editable={false}
                  value={dobFormat(user.dateOfBirthday || '')}
                />
              </View>
             
              <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this.handleDatePicked}
                onCancel={this.togglePicker}
                date={new Date(user.dateOfBirthday || '')}
              />
            </ScrollView>
          </ImageBackground>
        </SafeView>
      );
    }
}

const styles = StyleSheet.create({
  bgImageContainer: {
    flex: 1,
    width: '100%',
  },
  cameraViewContainer: {
    position: 'absolute',
    borderWidth: 2,
    borderColor: '#fff',
    height: 42,
    width: 42,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    right: 0,
    bottom: 17,
    backgroundColor: '#0F033E',
  },
  imageStyle: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
  },
  headerStyle: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  leftButton: {
    width: '20%',
  },
  centerTitle: {
    width: '60%',
    color: '#fff',
    fontSize: 14,
  },
  rightButton: {
    width: '20%',
    justifyContent: 'flex-end',
  },
  rightText: {
    color: '#fff',
    fontSize: 14,
  },
  topContainer: {
    height: height / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

EditProfile.propTypes = {
  getUser: PropTypes.func,
  updateProfile: PropTypes.func,
  user: PropTypes.object,
  navigation: PropTypes.object,
  editphase: PropTypes.string,
};

export default EditProfile;
