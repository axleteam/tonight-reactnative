import { connect } from 'react-redux';
import EditProfile from './EditProfile';
import { getProfile, updateProfile } from '../../store/profile/duck';

const mapStateToProps = ({ profileStore }) => profileStore;

const mapDispatchToProps = {
  getUser: () => getProfile(),
  updateProfile: user => updateProfile(user),
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
