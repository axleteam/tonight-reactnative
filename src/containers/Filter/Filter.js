import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SafeView from 'react-native-safe-area-view';
import { FlatList, View, ImageBackground, TouchableOpacity, StatusBar } from 'react-native';
import { Header, Left, Right, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ListItem from '../../components/ListItem';
import globalStyles from '../../assets/globalcss';
import bgImage from '../../../img/login_bg.png';

class Filter extends Component {
  static navigationOptions = () => ({
    header: null,
  });

  state = { selected: new Map() };

  componentWillMount() {
    this.props.getTags();
    const { eventsStore } = this.props;
    if (typeof eventsStore.filters.tagsIds !== 'undefined') {
      eventsStore.filters.map(id => this.onPressItem(id));
    }
  }


  onPressItem = (id) => {
    this.setState((state) => {
      const selected = new Map(state.selected);
      if (selected.get(id)) {
        selected.delete(id);
      } else {
        selected.set(id, !selected.get(id));
      }
      return { selected };
    });
  };

  keyExtractor = item => item.id;

  goBack = () => {
    this.props.navigation.goBack();
  }

  filter = () => {
    let { eventsStore: { filters } } = this.props;
    filters = {
      ...filters,
      tagsIds: [...this.state.selected.keys()],
    };
    this.props.getEventsByFilter(filters);
    this.props.getPlacesByFilter([...this.state.selected.keys()]);
    this.goBack();
  }
  renderItem = ({ item }) => (
    <ListItem
      id={item.id}
      onPressItem={this.onPressItem}
      selected={!!this.state.selected.get(item.id)}
      title={item.name}
    />
  );

  render() {
    const { tagsStore } = this.props;
    const { safeViewStyle } = globalStyles;
    const {
      mainViewStyle, textStyle, headerStyle, filterView,
    } = styles;
    return (
      <SafeView style={safeViewStyle} forceInset={{ bottom: 'never' }}>
        <View style={mainViewStyle}>
          <ImageBackground source={bgImage} style={{ flex: 1 }}>
            <Header style={headerStyle}>
              <StatusBar backgroundColor="rgba(0,0,0,0.5)" barStyle="light-content" />
              <Left>
                <TouchableOpacity onPress={this.goBack}>
                  <Icon name="chevron-left" size={30} color="#fff" />
                </TouchableOpacity>
              </Left>
              <Right>
                <TouchableOpacity onPress={this.filter}>
                  <Text style={textStyle}>Готово</Text>
                </TouchableOpacity>
              </Right>
            </Header>
            <View style={filterView}>
              <FlatList
                data={tagsStore.tags}
                renderItem={this.renderItem}
                extraData={this.state}
                keyExtractor={this.keyExtractor}
                showsVerticalScrollIndicator={false}
                contentInset={{ bottom: 50 }}
              />
            </View>
          </ImageBackground>
        </View>
      </SafeView>
    );
  }
}

Filter.propTypes = {
  getTags: PropTypes.func,
  getEventsByFilter: PropTypes.func,
  getPlacesByFilter: PropTypes.func,
  eventsStore: PropTypes.object,
  tagsStore: PropTypes.object,
  navigation: PropTypes.object,
};

const styles = {
  safeViewStyle: {
    flex: 1,
    backgroundColor: '#0F033E',
  },
  mainViewStyle: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  headerStyle: {
    paddingTop: 0,
    backgroundColor: 'transparent',
    height: 35,
    borderBottomWidth: 0,
  },
  textStyle: {
    color: '#fff',
    fontSize: 18,
  },
  filterView: {
    paddingRight: 5,
    flex: 1,
  },
  fixBackground: {
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    height: 100,
    zIndex: -1000,
  },
};


export default Filter;
