import { connect } from 'react-redux';
import Filter from './Filter';
import { getTags } from '../../store/tags/duck';
import { getEvents } from '../../store/event/duck';
import { getPlaces } from '../../store/places/duck';

const mapStateToProps = ({ tagsStore, eventsStore }) => (
  { tagsStore, eventsStore }
);

const mapDispatchToProps = {
  getTags: () => getTags(),
  getEventsByFilter: filter => getEvents(filter),
  getPlacesByFilter: filter => getPlaces(filter),
};

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
