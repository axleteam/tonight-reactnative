import React, { Component } from 'react';
import { View, ImageBackground, Text } from 'react-native';
import { Button, Title, Spinner } from 'native-base';
import PropTypes from 'prop-types';
import TonightInput from '../../components/forms/input';
import styles from '../LoginScreen/style';
import globalStyles from '../../assets/globalcss';
import { checkError } from '../../utils/methods';

const bgImage = require('../../../img/login_bg.png');

class ForgotScreen extends Component {
  static navigationOptions = () => ({
    title: 'Forgot Password',
    headerTintColor: '#fff',
    headerStyle: globalStyles.headerStyle,
    headerTitleStyle: globalStyles.headerTitleStyle,
    headerTransparent: true,
  });

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      formValues: {},
    };
  }


  handleSubmit = () => {
    const { formValues } = this.state;
    this.props.userForgot(formValues);
  };

  handleChange = (field, value) => {
    const formValues = { ...this.state.formValues };
    formValues[field] = value;
    this.setState(() => ({ formValues }));
  };

  render() {
    const {
      container,
      bgImageContainer,
      bottomView,
      buttonStyle,
      buttonFont,
      titleStyle,
      centerContent,
    } = styles;

    const { errorStyle, loader } = globalStyles;
    const { forgotStore } = this.props;
    const { errors, phase } = forgotStore;
    const { formValues } = this.state;
    return (
      <View style={container}>
        <ImageBackground style={[bgImageContainer, centerContent]} source={bgImage}>
          {phase === 'LOADING' ? <Spinner color="red" style={loader} /> : null}
          <View style={{ opacity: phase === 'LOADING' ? 0.5 : 1 }}>
            <View>
              <Title style={titleStyle}>Восстановить пароль</Title>
              <TonightInput
                value={formValues.email || ''}
                placeholder="Введите Email"
                onChange={this.handleChange}
                name="email"
                error={checkError('Email', errors)}
              />
              {errors !== null && typeof errors.forgot_password_failure !== 'undefined' ? (
                <Text style={errorStyle}>{errors.forgot_password_failure}</Text>
            ) : null}
            </View>
            <View style={bottomView}>
              <Button style={buttonStyle} onPress={this.handleSubmit} disabled={forgotStore.phase === 'LOADING'}>
                <Text style={buttonFont}>Восстановить пароль</Text>
              </Button>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

ForgotScreen.propTypes = {
  userForgot: PropTypes.func,
  forgotStore: PropTypes.object,
};

export default ForgotScreen;
