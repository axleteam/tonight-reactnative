import { connect } from 'react-redux';
import ForgotScreen from './ForgotScreen';
import { forgotUser } from '../../store/auth/duck';

const mapStateToProps = ({ forgotStore }) => ({
  forgotStore,
});

const mapDispatchToProps = {
  userForgot: email => forgotUser(email),
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotScreen);
