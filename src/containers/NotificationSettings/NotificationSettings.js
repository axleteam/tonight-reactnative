import React, { Component } from 'react';
import { ImageBackground, ScrollView } from 'react-native';
import SafeView from 'react-native-safe-area-view';
import { List, View } from 'native-base';
import globalStyles from '../../assets/globalcss';
import bgImage from '../../../img/login_bg.png';
import ToggleItem from '../../components/forms/toggleItem';
import TonightCheckbox from '../../components/forms/checkbox';

class NotificationSettings extends Component {
    static navigationOptions = () => ({
      title: 'Уведомления',
      headerTintColor: '#fff',
      headerStyle: globalStyles.headerStyle,
      headerTitleStyle: globalStyles.headerTitleStyle,
      headerTransparent: true,
    });

    render() {
      const { safeViewStyle, bgImageContainer } = globalStyles;
      return (
        <SafeView style={safeViewStyle}>
          <ImageBackground style={bgImageContainer} source={bgImage}>
            <ScrollView>
              <List style={{ paddingTop: 80, backgroundColor: 'transparent' }}>
                <ToggleItem marginTop={3} label="Все" value={false} />
                <ToggleItem marginTop={3} label="Важное" value={false} text="Краткое описание подкатигории уведомлений." />
                <ToggleItem label="Фестивали" value={false} text="Краткое описание подкатигории уведомлений." />
                <ToggleItem label="Клубы/Места" value text="Краткое описание подкатигории уведомлений." />
                <ToggleItem label="Фильтры" value={false} text="Краткое описание подкатигории уведомлений." />
                <View style={{ marginTop: 20 }}>
                  <TonightCheckbox label="Напомнить за день." checked />
                  <TonightCheckbox label="Напомнить за 6 часов." checked={false} />
                </View>
              </List>
            </ScrollView>
          </ImageBackground>
        </SafeView>
      );
    }
}

export default NotificationSettings;
