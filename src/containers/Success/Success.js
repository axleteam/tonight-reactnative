import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ImageBackground, View, Image, Text, StyleSheet } from 'react-native';
import { Container, CheckBox, Button } from 'native-base';

import bgImage from '../../../img/login_bg.png';
import tick from '../../../img/tick.png';

class Success extends Component {
  static navigationOptions = () => ({
    header: null,
  });

  goBack = () => {
    this.props.navigation.goBack();
  }

  render() {
    const {
      textStyle,
      checkboxTextStyle,
      viewCheckbox,
      checkboxStyle,
      buttonStyle,
      buttonText,
      btnContainer,
      tickIcon,
      bgImageContainer,
    } = styles;
    return (
      <Container style={{ backgroundColor: 'transparent' }}>
        <ImageBackground source={bgImage} style={bgImageContainer}>
          <Image source={tick} style={tickIcon} resizeMode="cover" />
          <View style={{ paddingTop: 24 }}>
            <Text style={textStyle}>Вы добавили</Text>
            <Text style={textStyle}>мероприятие в избранное!</Text>
          </View>
          <View style={{ paddingTop: 24 }}>
            <View style={viewCheckbox}>
              <CheckBox checked color="#a1047d" style={checkboxStyle} />
              <Text style={checkboxTextStyle}>Напомнить за день.</Text>
            </View>
            <View style={viewCheckbox}>
              <CheckBox checked color="#a1047d" style={checkboxStyle} />
              <Text style={checkboxTextStyle}>Напомнить за 6 часов.</Text>
            </View>
          </View>
          <View style={btnContainer}>
            <Button
              style={buttonStyle}
              onPress={this.goBack}
            >
              <Text style={buttonText}>
            Готово
              </Text>
            </Button>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

Success.propTypes = {
  navigation: PropTypes.object,
};


const styles = StyleSheet.create({
  bgImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 20,
  },
  checkboxTextStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
  },
  viewCheckbox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
  },
  checkboxStyle: {
    borderRadius: 5,
    marginRight: 15,
    alignSelf: 'center',
  },
  buttonStyle: {
    backgroundColor: '#a1047d',
    width: '100%',
  },
  buttonText: {
    fontSize: 24,
    color: '#fff',
    textAlign: 'center',
    flex: 1,
  },
  btnContainer: {
    margin: 20,
    marginTop: 100,
  },
  tickIcon: {
    height: 50,
    width: 65,
  },
});

export default Success;
