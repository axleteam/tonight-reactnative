import React, { Component } from 'react';
import { View, ImageBackground, Text, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import styles from './style';
import { LOGIN_USER } from '../../store/auth/duck';


const bgImage = require('../../../img/home_bg.png');
const logoImage = require('../../../img/tonight_logo.png');
const fbBg = require('../../../img/fb_bg.png');
const fbLogo = require('../../../img/fb_logo.png');

const FBSDK = require('react-native-fbsdk');

const {
  LoginManager,
  AccessToken,
} = FBSDK;
class HomeScreen extends Component {
  static navigationOptions = {
    header: null,
    headerStyle: { paddingTop: 20 },
  };

  state ={
    loaded: false,
  }

  componentWillMount = async () => {
    const value = await AsyncStorage.getItem(LOGIN_USER);
    if (value !== null) {
      this.props.navigation.navigate('Events');
      SplashScreen.hide();
      setTimeout((() => this.setState(() => ({ loaded: true }))), 1000);
    } else {
      this.setState(() => ({ loaded: true }));
      SplashScreen.hide();
    }
  }
  componentWillReceiveProps = (nextProps) => {
    if (nextProps.loginStore.phase === 'SUCCESS') {
      this.props.navigation.navigate('Events');
    }
  }

  onLoginPress = () => {
    this.props.navigation.navigate('Login');
  }

  onRegisterPress = () => {
    this.props.navigation.navigate('Register');
  }

  fbLogin = () => {
    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
      (result) => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            this.props.userLogin({ accessToken: data.accessToken.toString() });
          });
        }
      },
      () => {
        alert('Login failed');
      },
    );
  }
  render() {
    const {
      container,
      logoContainer,
      bgImageContainer,
      fbImageContainer,
      fbContainer,
      fbView,
      textStyle,
      emailView,
      fbViewSection,
      emailLink,
      bottomLinkView,
      textBold,
    } = styles;
    const { loaded } = this.state;
    return (
      loaded ?
        <View style={container}>
          <ImageBackground style={bgImageContainer} source={bgImage}>
            <View style={logoContainer}>
              <Image source={logoImage} />
            </View>

            <View style={fbContainer}>
              <View>
                <TouchableOpacity onPress={this.fbLogin}>
                  <ImageBackground style={fbImageContainer} source={fbBg}>
                    <View style={fbView}>
                      <View style={fbViewSection}>
                        <Image source={fbLogo} style={[container, { resizeMode: 'contain' }]} />
                      </View>
                      <View style={bottomLinkView}>
                        <Text style={textStyle}>
                        Продолжить через <Text style={textBold}>Facebook</Text>
                        </Text>
                      </View>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={emailLink}>
                <Text style={textStyle}>
                  Или продолжить через <Text style={textBold}>Email</Text>
                </Text>
              </View>
              <View style={emailView}>
                <TouchableOpacity onPress={this.onLoginPress}>
                  <Text style={[textStyle, textBold]}>Вход</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.onRegisterPress}>
                  <Text style={[textStyle, textBold]}>Регистрация</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        </View>
        :
        null
    );
  }
}

export default HomeScreen;
