import { connect } from 'react-redux';
import HomeScreen from './HomeScreen';
import { loginUser } from '../../store/auth/duck';

const mapStateToProps = ({ loginStore }) => ({
  loginStore,
});

const mapDispatchToProps = {
  userLogin: cred => loginUser(cred),
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
