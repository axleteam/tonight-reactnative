import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  logoContainer: {
    flex: 1,
    marginTop: 55,
    alignItems: 'center',
  },
  bgImageContainer: {
    flex: 1,
    width: '100%',
  },
  fbImageContainer: {
    height: 38,
  },
  fbContainer: {
    flex: 1,
    marginTop: height / 2.3,
    width,
    paddingHorizontal: 40,
  },
  fbView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  emailView: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 30,
    justifyContent: 'space-between',
  },
  textStyle: {
    color: '#fff',
  },
  imageLogo: {
    marginTop: 10,
  },
  fbViewSection: {
    paddingLeft: 20,
  },
  emailLink: {
    alignItems: 'center',
    paddingTop: 30,
  },
  bottomLinkView: {
    marginLeft: 25,
    flex: 1,
  },
  textBold: {
    fontWeight: '800',
  },
});

export default styles;
