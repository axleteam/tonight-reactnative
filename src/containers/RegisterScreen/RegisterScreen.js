import React, { Component } from 'react';
import { View, ImageBackground, Text } from 'react-native';
import { Button, Spinner } from 'native-base';
import PropTypes from 'prop-types';
import styles from './style';
import TonightInput from '../../components/forms/input';
import globalStyles from '../../assets/globalcss';
import { checkError } from '../../utils/methods';

const bgImage = require('../../../img/login_bg.png');

class RegisterScreen extends Component {
  static navigationOptions = () => ({
    title: 'Register',
    headerTintColor: '#fff',
    headerStyle: globalStyles.headerStyle,
    headerTitleStyle: globalStyles.headerTitleStyle,
    headerTransparent: true,
  });

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      formValues: {
        confirmPassword: '',
      },
      confirmPasswordError: null,
    };
  }
  componentWillReceiveProps = (nextProps) => {
    if (nextProps.registerStore.phase === 'SUCCESS') {
      alert('Register Successfully');
      this.props.navigation.navigate('Login');
    }
  }

  handleSubmit = () => {
    const { formValues } = this.state;
    if (formValues.Password && formValues.Password !== formValues.confirmPassword) {
      this.setState({ confirmPasswordError: 'Password Not Match' });
      return false;
    }
    this.setState({ confirmPasswordError: null });
    delete formValues.confirmPassword;
    return this.props.userRegister(formValues);
  };

  handleChange = (field, value) => {
    const formValues = { ...this.state.formValues };
    formValues[field] = value;
    this.setState(() => ({ formValues }));
  };


  render() {
    const {
      container,
      bgImageContainer,
      bottomView,
      buttonStyle,
      buttonFont,
    } = styles;

    const { errorStyle, loader } = globalStyles;
    const { registerStore } = this.props;
    const { confirmPasswordError, formValues } = this.state;
    const { errors, phase } = registerStore;

    return (
      <View style={container}>
        <ImageBackground style={bgImageContainer} source={bgImage}>
          {phase === 'LOADING' ? <Spinner color="red" style={loader} /> : null}
          <View style={{ opacity: phase === 'LOADING' ? 0.5 : 1 }}>
            <View style={{ paddingTop: 80 }}>
              <TonightInput
                value={formValues.FirstName || ''}
                placeholder="Имя"
                onChange={this.handleChange}
                name="FirstName"
                error={checkError('FirstName', errors)}
              />
              <TonightInput
                value={formValues.LastName || ''}
                placeholder="Фамилия"
                onChange={this.handleChange}
                name="LastName"
                error={checkError('LastName', errors)}
              />
              <TonightInput
                value={formValues.Email || ''}
                placeholder="Эл. адрес"
                onChange={this.handleChange}
                name="Email"
                error={checkError('Email', errors)}
              />
              {errors !== null && typeof errors.DuplicateUserName !== 'undefined' ? (
                <Text style={errorStyle}>{errors.DuplicateUserName}</Text>
            ) : null}
              <TonightInput
                value={formValues.Password || ''}
                placeholder="Пароль"
                onChange={this.handleChange}
                name="Password"
                error={checkError('Password', errors)}
                secret
              />
              {errors !== null && typeof errors.PasswordTooShort !== 'undefined' ? (
                <Text style={errorStyle}>{errors.PasswordTooShort}</Text>
            ) : null}
              <TonightInput
                value={formValues.confirmPassword || ''}
                placeholder="Подтвердите пароль"
                onChange={this.handleChange}
                name="confirmPassword"
                error={confirmPasswordError}
                secret
              />
              {errors !== null && typeof errors.register_failure !== 'undefined' ? (
                <Text style={errorStyle}>{errors.register_failure}</Text>
            ) : null}
            </View>
            <View style={bottomView}>
              <Button style={buttonStyle} onPress={this.handleSubmit} disabled={phase === 'LOADING'}>
                <Text style={buttonFont}>Регистрация</Text>
              </Button>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

RegisterScreen.propTypes = {
  registerStore: PropTypes.object,
  navigation: PropTypes.object,
  userRegister: PropTypes.func,
};

export default RegisterScreen;
