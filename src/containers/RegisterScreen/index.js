import { connect } from "react-redux";
import RegisterScreen from './RegisterScreen';
import { registerUser } from "../../store/auth/duck";

const mapStateToProps = ({registerStore}) => {
  return {
    registerStore
  };
};

const mapDispatchToProps = {
  userRegister: formdata => registerUser(formdata)
};

export default connect(mapStateToProps,mapDispatchToProps)(RegisterScreen);
