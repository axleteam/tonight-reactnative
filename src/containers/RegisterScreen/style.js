import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  bgImageContainer: {
    flex: 1,
    width: '100%',
  },
  bottomView: {
    width,
    alignItems: 'center',
  },
  buttonStyle: {
    width: '85%',
    justifyContent: 'center',
    marginTop: 20,
    backgroundColor: '#A7007A',
    borderRadius: 10,
    alignSelf: 'center',
  },
  buttonFont: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '800',
  },
  bottomLink: {
    color: '#fff',
    fontSize: 16,
    marginTop: 10,
  },
  inputMargin: {
    marginTop: 10,
  },
});

export default styles;
