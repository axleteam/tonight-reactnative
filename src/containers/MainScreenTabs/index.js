import React from 'react';
import { TabNavigator } from 'react-navigation';
import { Image } from 'react-native';
import Events from './Events';
import Account from './Account';
import Search from './Search';
import Favourite from './Favourite';
import eventIcon from '../../../img/Speaker_Icon.png';
import searchIcon from '../../../img/Magnifier_icon.png';
import favIcon from '../../../img/Heart_Icon.png';
import usericon from '../../../img/Profile_Icon.png';
import eventActiveIcon from '../../../img/Speaker_Icon_full.png';
import searchActiveIcon from '../../../img/Magnifier_icon_full.png';
import favActiveIcon from '../../../img/Heart_Icon_full.png';
import userActiveicon from '../../../img/Profile_Icon_full.png';

const EventTabs = new TabNavigator({
  Events: {
    screen: Events,
    navigationOptions: {
      tabBarIcon: ({ focused }) =>
        (<Image
          style={{ width: 30, height: 30 }}
          source={focused ? eventActiveIcon : eventIcon}
        />),
    },
  },
  Search: {
    screen: Search,
    navigationOptions: {
      tabBarIcon: ({ focused }) =>
        (<Image
          style={{ width: 30, height: 29 }}
          source={focused ? searchActiveIcon : searchIcon}
        />),
    },
  },
  Favourite: {
    screen: Favourite,
    navigationOptions: {
      tabBarIcon: ({ focused }) =>
        (<Image
          style={{ width: 30, height: 29 }}
          source={focused ? favActiveIcon : favIcon}
        />),
    },
  },
  User: {
    screen: Account,
    navigationOptions: {
      tabBarIcon: ({ focused }) =>
        (<Image
          style={{ width: 30, height: 29 }}
          source={focused ? userActiveicon : usericon}
        />),
    },
  },
}, {
  tabBarOptions: {
    showLabel: false,
    showIcon: true,
    style: { backgroundColor: '#fff' },
    indicatorStyle: {
      opacity: 0,
    },
  },
  animationEnabled: false,
  initialRouteName: 'Events',
  tabBarPosition: 'bottom',
  swipeEnabled: false,
});
export default EventTabs;

