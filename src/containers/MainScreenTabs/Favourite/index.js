import { connect } from 'react-redux';
import Favourite from './Favourite';
import { getFavouriteEvents, getFavouritePlaces } from '../../../store/favourite/duck';

const mapStateToProps = ({ favouriteEventsStore, favouritePlacesStore }) => (
  { favouriteEventsStore, favouritePlacesStore }
);

const mapDispatchToProps = {
  getFavouriteEvents: () => getFavouriteEvents(),
  getFavouritePlaces: () => getFavouritePlaces(),
};
export default connect(mapStateToProps, mapDispatchToProps)(Favourite);
