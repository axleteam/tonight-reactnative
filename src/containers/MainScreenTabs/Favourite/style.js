import { Dimensions, Platform } from 'react-native';

const { width } = Dimensions.get('window');
const styles = {
  mainView: {
    width: '50%',
    padding: 2,
    paddingTop: 0,
  },
  viewWrap: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  titleStyle: {
    color: '#bf178e',
    fontSize: 16,
  },
  headerBody: {
    borderBottomColor: '#bf178e',
    borderBottomWidth: 2,
    alignItems: 'center',
    marginLeft: Platform.OS === 'ios' ? 0 : width / 5,
    flex: 0,
    paddingHorizontal: 5,
  },
  headerStyle: {
    backgroundColor: '#0F033E',
    borderBottomWidth: 0,
    height: 30,
    paddingTop: 0,
  },
};

const tabStyle = {
  tabStyle: { backgroundColor: '#0F033E' },
  textStyle: { color: '#fff' },
  activeTabStyle: { backgroundColor: '#0F033E' },
  activeTextStyle: { color: '#fff' },
};

export { styles, tabStyle };
