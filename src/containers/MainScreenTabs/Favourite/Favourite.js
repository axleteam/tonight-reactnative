import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StatusBar,
  Text,
  Dimensions,
  ScrollView,
} from 'react-native';
import SafeView from 'react-native-safe-area-view';
import { Header, Body, Tabs, Tab, Title, ScrollableTab, Left, Right, Spinner } from 'native-base';
import { styles, tabStyle } from './style';
import globalStyles from '../../../assets/globalcss';
import BannerItem from '../../../components/BannerItem';
import imgShadow from '../../../../img/img_shadow.png';
import blackShadow from '../../../../img/img_shadow_black.png';

const { height, width } = Dimensions.get('window');
let indexColor = true;

class Favourite extends Component {
  static navigationOptions = () => ({
    header: null,
    gesturesEnabled: false,
  });

  componentDidMount() {
    this.props.getFavouriteEvents();
  }

  getColor = (index) => {
    if (index === 0) { return imgShadow; }
    if (indexColor) {
      if (index % 2) {
        indexColor = false;
        return blackShadow;
      }
      return imgShadow;
    }
    if (index % 2) {
      indexColor = true;
      return imgShadow;
    }
    return blackShadow;
  };

  changeTab = ({ i }) => {
    if (i === 1) { return this.props.getFavouritePlaces(); }
    return this.props.getFavouriteEvents();
  }


  render() {
    const {
      titleStyle,
      headerBody,
      headerStyle,
      viewWrap,
      mainView,
    } = styles;
    const { safeViewStyle, loader } = globalStyles;
    const { favouriteEventsStore, favouritePlacesStore } = this.props;
    const { events, phase } = favouriteEventsStore;
    const { places } = favouritePlacesStore;

    return (
      <SafeView style={safeViewStyle}>
        <Header style={headerStyle} hasTabs>
          <StatusBar backgroundColor="rgba(0,0,0,0.5)" barStyle="light-content" />
          <Left />
          <Body style={headerBody}>
            <Title> <Text style={titleStyle}>ИЗБРАННОЕ</Text></Title>
          </Body>
          <Right />
        </Header>
        <Tabs renderTabBar={() => <ScrollableTab />} tabBarBackgroundColor="#0F033E" tabBarUnderlineStyle={{ backgroundColor: '#bf178e' }} onChangeTab={this.changeTab}>
          <Tab heading="ИВЕНТЫ" {...tabStyle}>
            {(phase === 'LOADING' && events.length === 0) && <Spinner color="red" style={loader} />}
            <ScrollView>
              {events.map((event, index) =>
            (
              <BannerItem
                item={event.event}
                width="100%"
                height={height / 4}
                tags
                index={index}
                key={event.event.uid}
              />

          ))}
            </ScrollView>
          </Tab>
          <Tab heading="ЛОКАЦИИ" {...tabStyle}>
            {(favouritePlacesStore.phase === 'LOADING' && places.length === 0) && <Spinner color="red" style={loader} />}
            <ScrollView>
              <View style={viewWrap}>
                {places.map((place, index) =>
            (
              <View style={mainView} key={place.place.uid}>
                <BannerItem
                  item={place.place}
                  width="100%"
                  height={width / 2.1}
                  tags
                  index={index}
                  place
                  shadow={this.getColor(index)}
                  date={false}
                />
              </View>

          ))}
              </View>
            </ScrollView>
          </Tab>
        </Tabs>
        <View />
      </SafeView>
    );
  }
}

Favourite.propTypes = {
  getFavouriteEvents: PropTypes.func,
  getFavouritePlaces: PropTypes.func,
  favouriteEventsStore: PropTypes.object,
  favouritePlacesStore: PropTypes.object,
};

export default Favourite;
