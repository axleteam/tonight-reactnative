import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StatusBar,
  Text,
  Modal,
  BackHandler,
} from 'react-native';
import SafeView from 'react-native-safe-area-view';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Header, Left, Body, Right, Button, Tabs, Title, Spinner } from 'native-base';
import { styles, tabStyle } from './style';
import EventTab from '../../../components/EventTab';
import TonightTabBar from '../../../components/TonightTabBar';
import globalStyles from '../../../assets/globalcss';
import TonightCalendar from '../../../components/TonightCalendar';
import { isoToDate, getDayMonth } from '../../../utils/methods';

class Events extends Component {
  static navigationOptions = () => ({
    header: null,
    gesturesEnabled: false,
  });

  state={
    locationEnabled: false,
    modalVisible: false,
  }

  componentDidMount = () => {
    this.props.getCategories();
    this.props.getPlaces({});
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  toggleCalendar = () => {
    this.setState(() => ({ modalVisible: !this.state.modalVisible }));
  }

  filterByDate= ({ dateString }) => {
    let { eventsStore: { filters } } = this.props;
    const date = new Date(dateString);
    date.setDate(date.getDate() + 1); // Add One Day
    filters = {
      ...filters,
      startDateFrom: new Date(dateString).toISOString(),
      startDateTo: date.toISOString(),
    };
    this.props.getEvents(filters);
    this.toggleCalendar();
  }

  changeTab = ({ i }) => {
    const { categoryStore, eventsStore: { filters } } = this.props;

    if (!categoryStore.categories.length) {
      return true;
    }
    const { name } = categoryStore.categories[i];
    switch (name) {
      case 'Location':
        this.props.getPlaces(filters);
        break;
      case 'Event':
        this.props.getEvents(filters);
        break;
      default:
        return false;
    }
    return true;
  }

  goToFilter = () => {
    const { navigation } = this.props;
    navigation.navigate('Filter');
  }

  locationView = () => {
    this.setState(() => ({ locationEnabled: !this.state.locationEnabled }));
  }


  render() {
    const {
      titleStyle,
      headerBody,
      headerStyle,
      leftIconContainer,
      dateLeft,
    } = styles;
    const { categoryStore, navigation, eventsStore: { filters } } = this.props;
    const { locationEnabled, modalVisible } = this.state;
    const { phase } = categoryStore;
    const { loader, safeViewStyle } = globalStyles;

    return (
      <SafeView style={safeViewStyle}>
        <Header style={headerStyle} hasTabs>
          <StatusBar backgroundColor="rgba(0,0,0,0.5)" barStyle="light-content" />
          <Left style={leftIconContainer}>
            <Button transparent onPress={this.toggleCalendar}>
              <Icon name="calendar" color="#fff" size={20} />
            </Button>
            {filters.startDateFrom &&
            <Text style={dateLeft}>{getDayMonth(filters.startDateFrom)}
            </Text>
            }
          </Left>
          <Body style={headerBody}>
            <Title> <Text style={titleStyle}>КИЕВ</Text></Title>
          </Body>
          <Right>
            <Button transparent onPress={this.goToFilter}>
              <Icon name="tune" color="#fff" size={20} />
            </Button>
            <Button transparent onPress={this.locationView}>
              <Icon name="map-marker-radius" color={locationEnabled ? '#bf178e' : '#fff'} size={20} />
            </Button>
          </Right>
        </Header>
        {phase === 'LOADING' ? <Spinner color="red" style={loader} /> : null}
        <Tabs
          locked={locationEnabled}
          renderTabBar={() => <TonightTabBar />}
          tabBarUnderlineStyle={{ backgroundColor: '#bf178e' }}
          onChangeTab={this.changeTab}
          style={{ opacity: modalVisible ? 0.4 : 1 }}
        >
          {categoryStore.categories.map(category =>
            (<EventTab
              heading={category.name}
              {...tabStyle}
              key={category.id}
              navigation={navigation}
              location={locationEnabled}
            />))}
        </Tabs>
        <Modal
          animationType="slide"
          transparent
          visible={modalVisible}
          onRequestClose={this.toggleCalendar}
        >
          <TonightCalendar
            onHide={this.toggleCalendar}
            onPressDay={this.filterByDate}
            selectedDate={isoToDate(filters.startDateFrom) || undefined}
          />
        </Modal>
      </SafeView>
    );
  }
}

Events.propTypes = {
  getCategories: PropTypes.func,
  getPlaces: PropTypes.func,
  getEvents: PropTypes.func,
  eventsStore: PropTypes.object,
  categoryStore: PropTypes.object,
  navigation: PropTypes.object,
};

export default Events;
