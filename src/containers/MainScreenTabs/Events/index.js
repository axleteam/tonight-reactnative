import { connect } from 'react-redux';
import Events from './Events';
import { getEvents } from '../../../store/event/duck';
import { getCategory } from '../../../store/categories/duck';
import { getPlaces } from '../../../store/places/duck';

const mapStateToProps = ({ categoryStore, eventsStore }) => (
  { categoryStore, eventsStore }
);

const mapDispatchToProps = {
  getEvents: id => getEvents(id),
  getCategories: () => getCategory(),
  getPlaces: id => getPlaces(id),
};

export default connect(mapStateToProps, mapDispatchToProps)(Events);
