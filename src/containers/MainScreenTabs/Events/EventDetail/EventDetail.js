import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import SafeView from 'react-native-safe-area-view';
import { Image, TouchableOpacity, Share, Animated } from 'react-native';
import { View, Text, Button } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import globalStyles from '../../../../assets/globalcss';
import { dateFormat, getTime, alert, checkImage } from '../../../../utils/methods';
import Map from '../../../../components/Map';
import { styles } from '../style';
import shadowImg from '../../../../../img/rectangle_2.png';
import ImageScroll from '../../../../components/ImageScroll';

class EventDetail extends Component {
  static navigationOptions = () => ({
    header: null,
  });

  constructor(props) {
    super(props);
    if (this.props.event === undefined) {
      this.props.getEventDetail(props.navigation.state.params.id);
    }
  }

  state = {
    descriptionHeight: null,
    maxDescriptionHeight: null,
    show: false,
    textOpacity: new Animated.Value(1),
  };


  setMaxHeight = (event) => {
    if (this.state.maxDescriptionHeight === null) {
      const { height } = event.nativeEvent.layout;
      this.setState(() => ({
        maxDescriptionHeight: height,
        descriptionHeight: new Animated.Value(100),
      }));
    }
  }

  goBack = () => {
    this.props.navigation.goBack();
  }

  shareEvent = () => {
    const { event } = this.props;
    Share.share({
      title: event.name === null ? ' No Title' : event.name,
      message: event.description === null ? 'No Desc' : event.description,
      subject: event.name === null ? 'Name' : event.name,
    }).then(res => console.log(res));
  }

  showDescription = () => {
    Animated.parallel([
      // after decay, in parallel:
      Animated.timing(this.state.descriptionHeight, {
        toValue: this.state.maxDescriptionHeight,
        duration: 1000,
      }),
      Animated.timing(this.state.textOpacity, {
        toValue: 0,
        duration: 1000,
      }),
    ]).start();

    this.setState(() => ({ show: true }));
  }

  addFavourite = async ({ id }) => {   
    const events = this.toggleFavourite(true, id);
    alert('Added to Favourite', 'success');
    await this.props.addFavouriteEvent({ data: { eventId: id }, events });
    this.props.navigation.navigate('Success');
  }

  removeFavourite= ({ id }) => {
    const events = this.toggleFavourite(false, id);
    alert('Removed from Favourite');
    this.props.removeFavouriteEvent({ data: { eventId: id }, events });
  }

  toggleFavourite = (value, id) => {
    const { events } = this.props.eventsStore;
    const index = events.findIndex(event => event.id === id);
    events[index].isPlannedForUser = value;
    return events;
  }


  render() {
    let { event } = this.props;
    if (event === undefined) {
      event = this.props.eventDetailStore.event;
    }
    const { descriptionHeight, show, textOpacity } = this.state;
    const {
      backButtonContainer,
      shareButtonContainer,
      tagsPosition,
      bottomView,
      titleView,
      titleViewInner,
      titleFont,
      topHeading,
      headingValue,
      viewSeperator,
      headColor,
      buttonText,
      borderSeperator,
      buttonStyle,
      buttonContainer,
      mapContainer,
      imgShadowStyle,
      imageContainer,
      sliderContainer,
      sliderHeadingText,
      sliderStyle,
    } = styles;
    const buttonColor = event.isPlannedForUser ? '#ff0000' : '#a1047d';

    const {
      label, labelColor, labelView, textCenter, safeViewStyle,
    } = globalStyles;
    return (
      <SafeView style={safeViewStyle}>
        <ParallaxScrollView
          backgroundColor="blue"
          parallaxHeaderHeight={300}
          renderBackground={() => (
            <View key="background">
              <Image source={checkImage(event.imagePath)} style={imageContainer} contain="cover" />
            </View>
      )}
          renderForeground={() => (
            <View style={{ flex: 1, marginTop: 20 }}>
              <TouchableOpacity style={backButtonContainer} onPress={this.goBack}>
                <Icon name="chevron-left" size={20} color="#fff" />
              </TouchableOpacity>
              <TouchableOpacity style={shareButtonContainer} onPress={this.shareEvent}>
                <Icon name="share-variant" size={20} color="#fff" />
              </TouchableOpacity>
              <View style={[labelView, tagsPosition]}>
                {event.tags && event.tags.map((tag, i) =>
              (
                <View style={label} key={i}>
                  <Text style={labelColor}>{tag}</Text>
                </View>
          ))}
              </View>
            </View>
      )}
        >
          <View style={bottomView}>
            <View style={borderSeperator}>
              <View style={[viewSeperator, titleView]}>
                <View style={titleViewInner}>
                  <Text style={titleFont}>{event.name}</Text>
                  <View style={{ marginTop: 10 }}>
                    <Text style={topHeading}>Где:</Text>
                    <Text style={[headingValue, headColor]}>{event.place}</Text>
                  </View>
                </View>
              </View>
              <View style={viewSeperator}>
                <Text style={topHeading}>Когда:</Text>
                <Text style={headingValue}>{dateFormat(event.startTime)}</Text>
              </View>
              <View style={viewSeperator}>
                <Text style={topHeading}>Во сколько:</Text>
                <Text style={headingValue}>{`${getTime(event.startTime)} - ${getTime(event.endTime)}`}</Text>
              </View>
              {event.description !== null ?
                <View style={viewSeperator}>
                  <View>
                    <Animated.Text
                      style={[headingValue, { height: descriptionHeight !== null ? descriptionHeight : 'auto' }]}
                      onLayout={this.setMaxHeight}
                    >
                      {event.description}
                    </Animated.Text>
                    <Animated.Image source={shadowImg} style={[imgShadowStyle, { opacity: textOpacity }]} resizeMode="cover" />
                  </View>
                  {!show ?
                    <TouchableOpacity onPress={this.showDescription}>
                      <Text style={[headColor, textCenter]}>Ещё</Text>
                    </TouchableOpacity> :
            null
              }
                </View>
          : null}

            </View>
            <Map lat={event.latitude} long={event.longtitude} style={mapContainer} />
            <View style={buttonContainer}>
              <Button
                rounded
                style={[buttonStyle, { backgroundColor: buttonColor }]}
                onPress={
                        event.isPlannedForUser ?
                        this.removeFavourite.bind(this, event) :
                        this.addFavourite.bind(this, event)}
              >
                <Text style={buttonText}>{event.isPlannedForUser ? 'Удалить' : 'Сохранить'}</Text>
              </Button>
            </View>
          </View>
          {event.similarEvents &&
            <View style={sliderContainer}>
              <Text style={sliderHeadingText}>Похожие</Text>
              <ImageScroll events={event.similarEvents} style={sliderStyle} />
            </View>
        }

        </ParallaxScrollView>
      </SafeView>
    );
  }
}

EventDetail.propTypes = {
  navigation: PropTypes.object,
  addFavouriteEvent: PropTypes.func,
  removeFavouriteEvent: PropTypes.func,
  getEventDetail: PropTypes.func,
  eventsStore: PropTypes.object,
  event: PropTypes.object,
  eventDetailStore: PropTypes.object,
};

export default EventDetail;
