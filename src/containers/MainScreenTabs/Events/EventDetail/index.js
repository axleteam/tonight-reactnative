import { connect } from 'react-redux';
import EventDetail from './EventDetail';
import { addFavouriteEvent, removeFavouriteEvent } from '../../../../store/favourite/duck';
import { getEventDetail } from '../../../../store/event/duck';

const mapStateToProps = ({ eventDetailStore, eventsStore }, props) => ({
  event: eventsStore.events.find(event => event.id === props.navigation.state.params.id),
  eventsStore,
  eventDetailStore,
});

const mapDispatchToProps = {
  getEventDetail: id => getEventDetail(id),
  addFavouriteEvent: data => addFavouriteEvent(data),
  removeFavouriteEvent: data => removeFavouriteEvent(data),
};

export default connect(mapStateToProps, mapDispatchToProps)(EventDetail);
