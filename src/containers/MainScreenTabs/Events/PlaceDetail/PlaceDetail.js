import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, TouchableOpacity, Share } from 'react-native';
import { connect } from 'react-redux';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import SafeView from 'react-native-safe-area-view';
import { View, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import globalStyles from '../../../../assets/globalcss';
import { checkImage, alert, openUrl } from '../../../../utils/methods';
import Map from '../../../../components/Map';
import { styles } from '../style';
import { addFavouritePlace, removeFavouritePlace } from '../../../../store/favourite/duck';

class PlaceDetail extends Component {
  static navigationOptions = () => ({
    header: null,
  });

  goBack = () => {
    this.props.navigation.goBack();
  }

  share() {
    Share.share({
      message: 'Share',
    }).then(res => console.log(res));
  }

  addFavourite = ({ id }) => {
    const places = this.updatePlaces(true, id);
    alert('Added to Favourite', 'success');
    this.props.addFavouritePlace({ data: { placeId: id }, places });
  }

  removeFavourite= ({ id }) => {
    const places = this.updatePlaces(false, id);
    alert('Removed from Favourite');
    this.props.removeFavouritePlace({ data: { placeId: id }, places });
  }

  updatePlaces = (value, id) => {
    const { places } = this.props.placesStore;
    const index = places.findIndex(place => place.id === id);
    places[index].isPlannedForUser = value;
    return places;
  }

  render() {
    const { place } = this.props;
    const {
      imageContainer,
      backButtonContainer,
      shareButtonContainer,
      tagsPosition,
      bottomView,
      titleView,
      titleViewInner,
      titleFont,
      topHeading,
      headingValue,
      viewSeperator,
      headColor,
      borderSeperator,
      mapContainer,
      linkColor,
    } = styles;
    const {
      label, labelColor, labelView, safeViewStyle,
    } = globalStyles;
    return (
      <SafeView style={safeViewStyle}>
        <ParallaxScrollView
          backgroundColor="blue"
          parallaxHeaderHeight={300}
          outputScaleValue={7}
          renderBackground={() => (
            <View key="background">
              <Image source={checkImage(place.imagePath)} style={imageContainer} contain="cover" />
            </View>
      )}
          renderForeground={() => (
            <View style={{ flex: 1, marginTop: 20 }}>
              <TouchableOpacity style={backButtonContainer} onPress={this.goBack}>
                <Icon name="chevron-left" size={20} color="#fff" />
              </TouchableOpacity>
              <View style={shareButtonContainer}>
                <TouchableOpacity
                  onPress={
                        place.isPlannedForUser ?
                        this.removeFavourite.bind(this, place) :
                        this.addFavourite.bind(this, place)}
                  style={{ marginRight: 20 }}
                >
                  <Icon
                    name={place.isPlannedForUser ? 'heart' : 'heart-outline'}
                    color={place.isPlannedForUser ? '#bf178e' : '#fff'}
                    size={25}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.share}>
                  <Icon name="share-variant" size={20} color="#fff" />
                </TouchableOpacity>
              </View>
              <View style={[labelView, tagsPosition]}>
                {place.tags && place.tags.map((tag, i) =>
              (
                <View style={label} key={i}>
                  <Text style={labelColor}>{tag}</Text>
                </View>
          ))}
              </View>
            </View>
      )}
        >
          <View style={bottomView}>
            <View style={borderSeperator}>
              <View style={[viewSeperator, titleView]}>
                <View style={titleViewInner}>
                  <Text style={titleFont}>{place.name}</Text>
                  <View style={{ marginTop: 10 }}>
                    <Text style={topHeading}>Адрес:</Text>
                    <Text style={[headingValue, headColor]}>{place.place}</Text>
                  </View>
                </View>
              </View>
              <View style={viewSeperator}>
                <Text style={topHeading}>Веб-сайт:</Text>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={openUrl.bind(this, place.url)}>
                  <Icon name="earth" size={16} color="#BFBFBF" />
                  <Text style={[headingValue, linkColor]}>{place.url}</Text>
                </TouchableOpacity>
              </View>
              <View style={viewSeperator}>
                <Text style={topHeading}>Соц.сети:</Text>
                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={openUrl.bind(this, place.url)}>
                  <Icon name="facebook-box" size={16} color="#213b87" />
                  <Text style={[headingValue, linkColor]}>{place.url}</Text>
                </TouchableOpacity>
              </View>
              <View style={viewSeperator}>
                <Text style={headingValue}>{place.description}</Text>
              </View>
            </View>
            <Map lat={37.78825} long={-122.4324} style={mapContainer} />
          </View>
        </ParallaxScrollView>
      </SafeView>
    );
  }
}

PlaceDetail.propTypes = {
  addFavouritePlace: PropTypes.func,
  removeFavouritePlace: PropTypes.func,
  placesStore: PropTypes.object,
  navigation: PropTypes.object,
  place: PropTypes.object,
};

const mapStateToProps = ({ placesStore }, props) => ({
  place: placesStore.places.find(place => place.id === props.navigation.state.params.id),
  placesStore,
});

const mapDispatchToProps = {
  addFavouritePlace: placeId => addFavouritePlace(placeId),
  removeFavouritePlace: placeId => removeFavouritePlace(placeId),
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaceDetail);
