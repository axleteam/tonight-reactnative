import { connect } from 'react-redux';
import PlaceDetail from './PlaceDetail';
import { getPlaceDetail } from '../../../../store/places/duck';

const mapStateToProps = ({ placeDetailStore }) => placeDetailStore;
const mapDispatchToProps = {
  getPlaceDetail: id => getPlaceDetail(id),
};
export default connect(mapStateToProps, mapDispatchToProps)(PlaceDetail);
