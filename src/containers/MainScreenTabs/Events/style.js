import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');
const styles = {
  container: {
    flex: 1,
    backgroundColor: '#0F033E',
    paddingTop: Platform.OS === 'ios' ? 0 : 20,
  },
  titleStyle: {
    color: '#bf178e',
    fontSize: 16,
  },
  headerBody: {
    borderBottomColor: '#bf178e',
    borderBottomWidth: 2,
    alignItems: 'center',
    marginLeft: Platform.OS === 'ios' ? 0 : width / 5,
    flex: 0,
    paddingHorizontal: 5,
  },
  imageContainer: {
    width,
    height: height / 2,
  },
  parallexHeight: height / 2,
  backButtonContainer: {
    paddingTop: 10,
    paddingLeft: 10,
    position: 'absolute',
    backgroundColor: 'transparent',
  },
  shareButtonContainer: {
    paddingTop: 10,
    right: 10,
    position: 'absolute',
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  tagsPosition: {
    bottom: 15,
  },
  bottomView: {
    paddingLeft: 15,
    paddingRight: 15,
  },
  titleView: {
    marginTop: 10,
  },
  titleViewInner: {
    paddingTop: 15,
  },
  titleFont: {
    fontFamily: 'HelveticaNeue-Bold',
    fontSize: 20,
  },
  topHeading: {
    color: '#BFBFBF',
    fontSize: 14,
  },
  headColor: {
    color: '#c13c9b',
  },
  headingValue: {
    color: '#000',
    fontSize: 16,
  },
  viewSeperator: {
    borderLeftWidth: 2,
    borderRightWidth: 2,
    borderColor: '#EEEEEE',
    paddingLeft: 10,
    marginTop: 10,
    paddingRight: 10,
  },
  buttonText: {
    flex: Platform.OS === 'ios' ? 1 : 0,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  borderSeperator: {
    borderBottomWidth: 2,
    borderBottomColor: '#EEEEEE',
  },
  buttonStyle: {
    width: '100%',
    alignItems: 'center',
    flex: 1,
  },
  buttonContainer: {
    padding: 15,
    flex: 1,
  },
  mapContainer: {
    height: height / 5,
    marginTop: 20,
  },
  headerStyle: {
    backgroundColor: '#0F033E',
    borderBottomWidth: 0,
    height: 30,
    paddingTop: 0,
  },
  imgShadowStyle: {
    bottom: 0,
    position: 'absolute',
  },
  linkColor: {
    color: '#213b87',
    marginLeft: 5,
  },
  sliderContainer: {
    borderTopWidth: 2,
    borderTopColor: '#EEEEEE',
    paddingVertical: 12,
  },
  sliderHeadingText: {
    color: '#c13c9b',
    textAlign: 'center',
  },
  sliderStyle: {
    paddingLeft: 30, paddingVertical: 15,
  },
  leftIconContainer: {
    flexDirection: 'row',
  },
  dateLeft: {
    color: '#fff',
    paddingVertical: 6,
    alignSelf: 'center',
  },
};

const tabStyle = {
  tabStyle: { backgroundColor: '#0F033E' },
  textStyle: { color: '#fff' },
  activeTabStyle: { backgroundColor: '#0F033E' },
  activeTextStyle: { color: '#fff' },
};

export { styles, tabStyle };
