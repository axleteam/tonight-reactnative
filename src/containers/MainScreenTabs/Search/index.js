import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Keyboard, StyleSheet, TextInput, View, Text, TouchableOpacity, Dimensions, ImageBackground } from 'react-native';
import SafeView from 'react-native-safe-area-view';
import Icon from 'react-native-vector-icons/Ionicons';
import { Spinner } from 'native-base';
import Carousel from 'react-native-snap-carousel';
import globalStyles from '../../../assets/globalcss';
import BannerItem from '../../../components/BannerItem';
import bgImage from '../../../../img/login_bg.png';
import { searchEvents, clearSearch } from '../../../store/search/duck';
import { getPosition } from '../../../utils/methods';

const { width } = Dimensions.get('window');
const sliderImageWidth = width / 1.3;

class Search extends React.Component {
  static navigationOptions = () => ({
    header: null,
    gesturesEnabled: false,
  });

  constructor(props) {
    super(props);
    this.searchCarousel = {};
    this.placeCarousel = {};
    this.state = { searchText: '' };
  }

  search = () => {
    const { searchText } = this.state;
    this.props.getSearchEvents(searchText);
  }

  clearSearch = () => {
    this.props.clearSearch();
    this.setState(() => ({ searchText: '' }));
    Keyboard.dismiss();
  }

  renderItem = ({ item, index }) => (
    <BannerItem item={item} height={width / 2.2} width={sliderImageWidth} key={index} />
  );


    renderItem1 = ({ item, index }) =>
      (
        <BannerItem
          item={item}
          height={width / 2.2}
          width={sliderImageWidth}
          date={false}
          place
          key={index}
        />
      );

    render() {
      const { safeViewStyle, loader } = globalStyles;
      const {
        searchContainer,
        textInputStyle,
        searchResultContainer,
        textHeading,
        noFoundContainer,
        noFoundText,
      } = styles;
      const { searches, phase } = this.props.searchStore;
      const { events, places } = searches;
      return (
        <SafeView style={safeViewStyle}>
          {phase === 'LOADING' && <Spinner color="red" style={loader} />}
          <View style={{ paddingLeft: 15 }}>
            <View style={searchContainer}>
              <TextInput
                style={textInputStyle}
                onChangeText={searchText => this.setState(() => ({ searchText }))}
                placeholder="Введите название события или места"
                placeholderTextColor="gray"
                returnKeyType="search"
                onSubmitEditing={this.search}
                value={this.state.searchText}
              />
              <TouchableOpacity
                onPress={this.clearSearch}
                style={{ paddingRight: 10 }}
                hitSlop={getPosition(20, 10)}
              >
                <Icon name="md-close" size={20} color="#fff" />
              </TouchableOpacity>
            </View>
          </View>
          <ImageBackground source={bgImage} style={{ flex: 1 }}>
            {events.length > 0 &&
            <View style={searchResultContainer}>
              <View>
                <Text style={textHeading}>Рекомендованные ивенты</Text>
              </View>
              <View style={{ paddingTop: 20 }}>
                <Carousel
                  ref={(c) => { this.searchCarousel = c; }}
                  data={events}
                  renderItem={this.renderItem}
                  sliderWidth={width - 15}
                  itemWidth={sliderImageWidth}
                  layout="default"
                  firstItem={0}
                  activeSlideAlignment="start"
                />
              </View>
            </View>
            }

            {places.length > 0 &&
            <View style={searchResultContainer}>
              <View>
                <Text style={textHeading}>Клубы</Text>
              </View>
              <View style={{ paddingTop: 20 }}>
                <Carousel
                  ref={(c) => { this.placeCarousel = c; }}
                  data={places}
                  renderItem={this.renderItem1}
                  sliderWidth={width - 15}
                  itemWidth={sliderImageWidth}
                  layout="default"
                  firstItem={0}
                  activeSlideAlignment="start"
                />
              </View>
            </View>
            }

            {(phase === 'SUCCESS' && events.length === 0 && places.length === 0) &&
            <View style={noFoundContainer}>
              <Text style={noFoundText}>
                  Не найдено мест и событий за заданым поисковым словом. Попробуйте другое!
              </Text>
            </View>
            }
          </ImageBackground>
        </SafeView>
      );
    }
}

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 5,
    height: 50,
  },
  textInputStyle: {
    color: '#fff',
    fontSize: 14,
    width: '80%',
  },
  searchResultContainer: {
    paddingLeft: 15,
    paddingTop: 15,
    backgroundColor: 'transparent',
  },
  textHeading: {
    color: '#fff',
    fontSize: 14,
  },
  noFoundContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'transparent',
  },
  noFoundText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 16,
  },

});

const mapStateToProps = ({ searchStore }) => (
  { searchStore }
);

const mapDispatchToProps = {
  getSearchEvents: text => searchEvents(text),
  clearSearch: () => clearSearch(),
};

Search.propTypes = {
  getSearchEvents: PropTypes.func,
  clearSearch: PropTypes.func,
  searchStore: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
