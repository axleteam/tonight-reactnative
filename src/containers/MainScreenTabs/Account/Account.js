import React, { Component } from 'react';
import {
  View,
  AsyncStorage,
  StyleSheet,
  ImageBackground,
  Image,
} from 'react-native';
import SafeView from 'react-native-safe-area-view';
import PropTypes from 'prop-types';
import { List, Content } from 'native-base';
import { LOGIN_USER } from '../../../store/auth/duck';
import TonightListItem from '../../../components/TonightListItem';
import logoImage from '../../../../img/tonight_logo.png';
import bgImage from '../../../../img/login_bg.png';
import usericon from '../../../../img/profile_account.png';
import favicon from '../../../../img/favorite_account.png';
import headphoneIcon from '../../../../img/headphones_account.png';
import lockIcon from '../../../../img/lock_account.png';
import logoutIcon from '../../../../img/logout_account.png';
import paperIcon from '../../../../img/paper_account.png';
import contractIcon from '../../../../img/contract_account.png';
import globalStyles from '../../../assets/globalcss';

class Account extends Component {
  static navigationOptions = () => ({
    header: null,
  });

  logout = async () => {
    await AsyncStorage.removeItem(LOGIN_USER);
    this.props.navigation.navigate('Home');
  }

  goToSupport = () => {
    this.props.navigation.navigate('Support');
  }

  goToEditProfile = () => {
    this.props.navigation.navigate('EditProfile');
  }

  goToNotificationSettings = () => {
    this.props.navigation.navigate('NotificationSettings');
  }

  render() {
    const {
      bgImageContainer, logoContainer,
    } = styles;
    const { safeViewStyle } = globalStyles;
    return (
      <SafeView style={safeViewStyle}>
        <ImageBackground style={bgImageContainer} source={bgImage}>
          <Content>
            <View style={logoContainer}>
              <Image source={logoImage} />
            </View>
            <List style={bgImageContainer}>
              <TonightListItem name="Редактировать профиль" image={usericon} size={20} onPress={this.goToEditProfile} />
              <TonightListItem name="Поддержка" image={headphoneIcon} size={20} onPress={this.goToSupport} />
              <TonightListItem name="Оцени приложение" image={favicon} size={20} />
              <TonightListItem name="Уведомления" image={paperIcon} size={20} onPress={this.goToNotificationSettings} />
              <TonightListItem name="Политика приватности" image={lockIcon} size={20} />
              <TonightListItem name="Пользовательское соглашение" image={contractIcon} size={20} />
              <TonightListItem name="Выйти" image={logoutIcon} size={20} onPress={this.logout} />
            </List>
          </Content>
        </ImageBackground>
      </SafeView>
    );
  }
}

const styles = StyleSheet.create({
  bgImageContainer: {
    flex: 1,
    width: '100%',
  },
  logoContainer: {
    marginVertical: 55,
    alignItems: 'center',
  },
});

Account.propTypes = {
  navigation: PropTypes.object,
};

export default Account;
