import { API_URL } from '../../utils/constants';
import fetch from '../../utils/fetch';

export const getProfile = () => fetch(`${API_URL}Accounts/GetUser`, {
  method: 'GET',
}).then(res => res.json());

export const updateProfile = user => fetch(`${API_URL}Accounts`, {
  method: 'PUT',
  data: user,
  upload: true,
}).then(res => res.json());
