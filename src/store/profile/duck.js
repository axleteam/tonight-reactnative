import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';

export const GET_PROFILE_USER = 'tonight/profile/GET_PROFILE_USER';
export const GET_PROFILE_USER_SUCCESS = 'tonight/profile/GET_PROFILE_USER_SUCCESS';
export const GET_PROFILE_USER_ERROR = 'tonight/profile/GET_PROFILE_USER_ERROR';
export const GET_PROFILE_USER_CANCELLED = 'tonight/profile/GET_PROFILE_USER_CANCELLED';

export const UPDATE_PROFILE_USER = 'tonight/profile/UPDATE_PROFILE_USER';
export const UPDATE_PROFILE_USER_SUCCESS = 'tonight/profile/UPDATE_PROFILE_USER_SUCCESS';
export const CLEAR_UPDATE_PROFILE = 'tonight/profile/CLEAR_UPDATE_PROFILE';

const initialState = {
  phase: INIT,
  errors: null,
  user: {},
  editphase: INIT,
};

/*
  Reducer
*/
export function profileStore(state = initialState, action = {}) {
  switch (action.type) {
    case GET_PROFILE_USER:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_PROFILE_USER_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        user: action.payload,
      };
    case GET_PROFILE_USER_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
        editphase: ERROR,
      };
    case GET_PROFILE_USER_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
        editphase: INIT,
      };
    case UPDATE_PROFILE_USER:
      return {
        ...state,
        editphase: LOADING,
      };
    case UPDATE_PROFILE_USER_SUCCESS:
      return {
        ...state,
        editphase: SUCCESS,
        errors: null,
        user: action.payload,
      };
    case CLEAR_UPDATE_PROFILE:
      return {
        ...state,
        editphase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getProfile = () => ({
  type: GET_PROFILE_USER,
});

export const updateProfile = payload => ({
  type: UPDATE_PROFILE_USER,
  payload,
});

/*
  Epics
*/
/* eslint-disable */
export const profileUserEpic = action$ =>
  action$.ofType(GET_PROFILE_USER).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getProfile())
      .map(payload => {
        console.log(payload);
        return {
          type: GET_PROFILE_USER_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_PROFILE_USER_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_PROFILE_USER_CANCELLED));
  });

  export const updateUserEpic = action$ =>
  action$.ofType(UPDATE_PROFILE_USER)
    .flatMap(action =>
      Rx.Observable.concat(
        Rx.Observable.fromPromise(api.updateProfile(action.payload))
      .map(payload => {
        return {
          type: UPDATE_PROFILE_USER_SUCCESS,
          payload
        };
      }),
      Rx.Observable.of(action).delay(1000).map(()=>{
        return {
          type: CLEAR_UPDATE_PROFILE,
        };
      })
      )
      .takeUntil(action$.ofType(GET_PROFILE_USER_CANCELLED))
    );

/*
      Export Epics
    */
export const profileEpic = combineEpics(profileUserEpic);
export const updateProfileEpic = combineEpics(updateUserEpic);
