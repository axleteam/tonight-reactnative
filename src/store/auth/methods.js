import { AsyncStorage } from 'react-native';
import { API_URL } from '../../utils/constants';
import fetch from '../../utils/fetch';
import { LOGIN_USER } from './duck';

export const loginUser = (credentials) => {
  let url;
  if ('accessToken' in credentials) {
    url = `${API_URL}ExternalAuth/Facebook`;
  } else {
    url = `${API_URL}Auth/Login`;
  }
  return fetch(
    `${url}`,
    {
      method: 'POST',
      body: JSON.stringify(credentials),
    },
  ).then(res => res.json()).then(async (res) => {
    await AsyncStorage.setItem(LOGIN_USER, res);
    return JSON.parse(res);
  });
};

export const registerUser = formdata => fetch(`${API_URL}Accounts`, {
  method: 'POST',
  data: formdata,
}).then(res => res.json());

export const forgotUser = formdata => fetch(`${API_URL}Auth/forgot_password`, {
  method: 'POST',
  body: JSON.stringify(formdata),
}).then(res => res.json());
