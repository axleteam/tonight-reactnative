import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';

export const LOGIN_USER = 'tonight/login/LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'tonight/login/LOGIN_USER_SUCCESS';
export const LOGIN_USER_ERROR = 'tonight/login/LOGIN_USER_ERROR';
export const LOGIN_USER_CANCELLED = 'tonight/login/LOGIN_USER_CANCELLED';

export const REGISTER_USER = 'tonight/register/REGISTER_USER';
export const REGISTER_USER_SUCCESS = 'tonight/register/REGISTER_USER_SUCCESS';
export const REGISTER_USER_ERROR = 'tonight/register/REGISTER_USER_ERROR';
export const REGISTER_USER_CANCELLED = 'tonight/register/REGISTER_USER_CANCELLED';

export const FORGOT_USER = 'tonight/forgot/FORGOT_USER';
export const FORGOT_USER_SUCCESS = 'tonight/forgot/FORGOT_USER_SUCCESS';
export const FORGOT_USER_ERROR = 'tonight/forgot/FORGOT_USER_ERROR';
export const FORGOT_USER_CANCELLED = 'tonight/forgot/FORGOT_USER_CANCELLED';


const initialState = {
  phase: INIT,
  errors: null,
  user: null,
};

/*
  Reducer
*/
export function loginStore(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...state,
        phase: LOADING,
      };
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        user: action.payload,
      };
    case LOGIN_USER_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case LOGIN_USER_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const loginUser = credentials => ({
  type: LOGIN_USER,
  payload: credentials,
});

/*
  Epics
*/
/* eslint-disable */
export const loginUserEpic = action$ =>
  action$.ofType(LOGIN_USER).mergeMap(action => {
    return Rx.Observable.fromPromise(api.loginUser(action.payload))
      .map(payload => {
        console.log(payload);
        return {
          type: LOGIN_USER_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: LOGIN_USER_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(LOGIN_USER_CANCELLED));
  });

/* Register User  */

const registerInitialState = {
  phase: INIT,
  errors: null,
  user: null
};

/*
    Reducer
  */
export function registerStore(state = registerInitialState, action = {}) {
  switch (action.type) {
    case REGISTER_USER:
      return {
        ...state,
        phase: LOADING
      };
    case REGISTER_USER_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        user: action.payload
      };
    case REGISTER_USER_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error
      };
    case REGISTER_USER_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null
      };

    default: {
      return state;
    }
  }
}

/*
    Action creators
  */
export const registerUser = formdata => {
  return {
    type: REGISTER_USER,
    payload: formdata
  };
};

/*
    Epics
  */
/* eslint-disable */
export const registerUserEpic = action$ =>
  action$.ofType(REGISTER_USER).mergeMap(action => {
    return Rx.Observable.fromPromise(api.registerUser(action.payload))
      .map(payload => {
        console.log(payload)
        return {
          type: REGISTER_USER_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: REGISTER_USER_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(REGISTER_USER_CANCELLED));
  });


  /* Forgot Password  */

  const forgotInitialState = {
    phase: INIT,
    errors: null,
    user: null
  };
  
  /*
      Reducer
    */
  export function forgotStore(state = forgotInitialState, action = {}) {
    switch (action.type) {
      case FORGOT_USER:
        return {
          ...state,
          phase: LOADING
        };
      case FORGOT_USER_SUCCESS:
        return {
          ...state,
          phase: SUCCESS,
          errors: null,
          user: action.payload
        };
      case FORGOT_USER_ERROR:
        return {
          ...state,
          phase: ERROR,
          errors: action.payload.error
        };
      case FORGOT_USER_CANCELLED:
        return {
          ...state,
          phase: INIT,
          errors: null
        };
  
      default: {
        return state;
      }
    }
  }
  
  /*
      Action creators
    */
  export const forgotUser = formdata => {
    return {
      type: FORGOT_USER,
      payload: formdata
    };
  };
  
  /*
      Epics
    */
  /* eslint-disable */
  export const forgotUserEpic = action$ =>
    action$.ofType(FORGOT_USER).mergeMap(action => {
      return Rx.Observable.fromPromise(api.forgotUser(action.payload))
        .map(payload => {
          return {
            type: FORGOT_USER_SUCCESS,
            payload
          };
        })
        .catch(error =>
          Rx.Observable.of({
            type: FORGOT_USER_ERROR,
            payload: { error }
          })
        )
        .takeUntil(action$.ofType(FORGOT_USER_CANCELLED));
    });

/*
      Export Epics
    */
export const loginEpic = combineEpics(loginUserEpic);
export const registerEpic = combineEpics(registerUserEpic);
export const forgotEpic = combineEpics(forgotUserEpic);
