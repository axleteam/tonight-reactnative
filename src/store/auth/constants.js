export const LOGIN_USER = 'tonight/login/LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'tonight/login/LOGIN_USER_SUCCESS';
export const LOGIN_USER_ERROR = 'tonight/login/LOGIN_USER_ERROR';
export const LOGIN_USER_CANCELLED = 'tonight/login/LOGIN_USER_CANCELLED';


export const REGISTER_USER = 'tonight/register/REGISTER_USER';
export const REGISTER_USER_SUCCESS = 'tonight/register/REGISTER_USER_SUCCESS';
export const REGISTER_USER_ERROR = 'tonight/register/REGISTER_USER_ERROR';
export const REGISTER_USER_CANCELLED = 'tonight/register/REGISTER_USER_CANCELLED';
