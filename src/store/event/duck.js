import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';

export const GET_EVENTS = 'tonight/event/GET_EVENTS';
export const GET_EVENTS_SUCCESS = 'tonight/event/GET_EVENTS_SUCCESS';
export const GET_EVENTS_ERROR = 'tonight/event/GET_EVENTS_ERROR';
export const GET_EVENTS_CANCELLED = 'tonight/event/GET_EVENTS_CANCELLED';

export const GET_EVENTS_REGION = 'tonight/event/GET_EVENTS_REGION';
export const GET_EVENTS_REGION_SUCCESS = 'tonight/event/GET_EVENTS_REGION_SUCCESS';
export const GET_EVENTS_REGION_ERROR = 'tonight/event/GET_EVENTS_REGION_ERROR';
export const GET_EVENTS_REGION_CANCELLED = 'tonight/event/GET_EVENTS_REGION_CANCELLED';

export const GET_EVENT_DETAIL = 'tonight/event/GET_EVENT_DETAIL';
export const GET_EVENT_DETAIL_SUCCESS = 'tonight/event/GET_EVENT_DETAIL_SUCCESS';
export const GET_EVENT_DETAIL_ERROR = 'tonight/event/GET_EVENT_DETAIL_ERROR';
export const GET_EVENT_DETAIL_CANCELLED = 'tonight/event/GET_EVENT_DETAIL_CANCELLED';

const initialState = {
  phase: INIT,
  errors: null,
  events: [],
  filters: {},
};

/*
  Reducer
*/
export function eventsStore(state = initialState, action = {}) {
  switch (action.type) {
    case GET_EVENTS:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_EVENTS_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        events: action.payload,
        filters: action.filters === undefined ? [] : action.filters,
      };
    case GET_EVENTS_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_EVENTS_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getEvents = payload => ({
  type: GET_EVENTS,
  payload,
});

/*
  Epics
*/
/* eslint-disable */
export const getEventsEpic = action$ =>
  action$.ofType(GET_EVENTS).mergeMap(action => {
    console.log(action.payload);
    return Rx.Observable.fromPromise(api.getEvents(action.payload))
      .map(payload => {
        console.log(action.payload);
        return {
          type: GET_EVENTS_SUCCESS,
          filters:action.payload,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_EVENTS_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_EVENTS_CANCELLED));
  });


/* Event Detail Reducer */
const eventDetailInitialState = {
  phase: INIT,
  errors: null,
  event: [],
};

/*
  Reducer
*/
export function eventDetailStore(state = eventDetailInitialState, action = {}) {
  switch (action.type) {
    case GET_EVENT_DETAIL:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_EVENT_DETAIL_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        event: action.payload[0],
      };
    case GET_EVENT_DETAIL_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_EVENT_DETAIL_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getEventDetail= (payload) => ({
  type: GET_EVENT_DETAIL,
  payload
});

/*
  Epics
*/
/* eslint-disable */
export const getEventDetailEpic = action$ =>
  action$.ofType(GET_EVENT_DETAIL).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getEventDetail(action.payload))
      .map(payload => {
        console.log(payload);
        return {
          type: GET_EVENT_DETAIL_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_EVENT_DETAIL_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_EVENT_DETAIL_CANCELLED));
  });


  /* Event Detail Reducer */
const eventRegionInitialState = {
  phase: INIT,
  errors: null,
  events: [],
};

/*
  Reducer
*/
export function eventsRegionStore(state = eventRegionInitialState, action = {}) {
  switch (action.type) {
    case GET_EVENTS_REGION:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_EVENTS_REGION_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        events: action.payload,
      };
    case GET_EVENTS_REGION_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_EVENTS_REGION_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getEventRegion= (payload) => ({
  type: GET_EVENTS_REGION,
  payload
});

/*
  Epics
*/
/* eslint-disable */
export const getEventRegionEpic = action$ =>
  action$.ofType(GET_EVENTS_REGION).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getEventsByRegion(action.payload))
      .map(payload => {
        console.log(payload);
        return {
          type: GET_EVENTS_REGION_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_EVENTS_REGION_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_EVENTS_REGION_CANCELLED));
  });
/*
      Export Epics
    */
export const eventsEpic = combineEpics(getEventsEpic);
export const eventsRegionEpic = combineEpics(getEventRegionEpic);
export const eventDetailEpic = combineEpics(getEventDetailEpic);
