import fetch from '../../utils/fetch';
import { API_URL } from '../../utils/constants';

export const getEvents = filter => (
  fetch(`${API_URL}Events/GetEventByFilter`, {
    method: 'POST',
    body: JSON.stringify(filter),
  }).then(res => res.json()));

export const getEventDetail = id => fetch(`${API_URL}Events/${id}`).then(res => res.json());

export const getEventsByRegion = region => (
  fetch(`${API_URL}Events/GetEventByRegion`, {
    method: 'POST',
    body: JSON.stringify(region),
  }).then(res => res.json()));
