import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';
import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';
import { GET_PLACES_SUCCESS } from '../places/duck';
import { GET_EVENTS_SUCCESS } from '../event/duck';

export const ADD_FAVOURITE_EVENT = 'tonight/favourite/ADD_FAVOURITE_EVENT';
export const REMOVE_FAVOURITE_EVENT = 'tonight/favourite/REMOVE_FAVOURITE_EVENT';
export const FAVOURITE_EVENT_SUCCESS = 'tonight/favourite/FAVOURITE_EVENT_SUCCESS';
export const FAVOURITE_EVENT_ERROR = 'tonight/favourite/FAVOURITE_EVENT_ERROR';
export const FAVOURITE_EVENT_CANCELLED = 'tonight/favourite/FAVOURITE_EVENT_CANCELLED';

export const GET_FAVOURITE_EVENT = 'tonight/favourite/GET_FAVOURITE_EVENT';
export const GET_FAVOURITE_EVENT_SUCCESS = 'tonight/favourite/GET_FAVOURITE_EVENT_SUCCESS';
export const GET_FAVOURITE_EVENT_ERROR = 'tonight/favourite/GET_FAVOURITE_EVENT_ERROR';
export const GET_FAVOURITE_EVENT_CANCELLED = 'tonight/favourite/GET_FAVOURITE_EVENT_CANCELLED';

export const ADD_FAVOURITE_PLACE = 'tonight/favourite/ADD_FAVOURITE_PLACE';
export const REMOVE_FAVOURITE_PLACE = 'tonight/favourite/REMOVE_FAVOURITE_PLACE';
export const FAVOURITE_PLACE_SUCCESS = 'tonight/favourite/FAVOURITE_PLACE_SUCCESS';
export const FAVOURITE_PLACE_ERROR = 'tonight/favourite/FAVOURITE_PLACE_ERROR';
export const FAVOURITE_PLACE_CANCELLED = 'tonight/favourite/FAVOURITE_PLACE_CANCELLED';

export const GET_FAVOURITE_PLACE = 'tonight/favourite/GET_FAVOURITE_PLACE';
export const GET_FAVOURITE_PLACE_SUCCESS = 'tonight/favourite/GET_FAVOURITE_PLACE_SUCCESS';
export const GET_FAVOURITE_PLACE_ERROR = 'tonight/favourite/GET_FAVOURITE_PLACE_ERROR';
export const GET_FAVOURITE_PLACE_CANCELLED = 'tonight/favourite/GET_FAVOURITE_PLACE_CANCELLED';

const initialState = {
  phase: INIT,
  errors: null,
  events: [],
};

/*
  Reducer
*/
export function favouriteEventStore(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_FAVOURITE_EVENT:
      return {
        ...state,
        phase: LOADING,
      };
    case REMOVE_FAVOURITE_EVENT:
      return {
        ...state,
        phase: LOADING,
      };
    case FAVOURITE_EVENT_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        events: action.payload,
      };
    case FAVOURITE_EVENT_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case FAVOURITE_EVENT_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const addFavouriteEvent = payload => ({
  type: ADD_FAVOURITE_EVENT,
  payload,
});

/*
  Epics
*/
/* eslint-disable */
export const addFavouriteEventEpic = action$ =>
  action$.ofType(ADD_FAVOURITE_EVENT).flatMap(action => 
    Rx.Observable.concat(
      Rx.Observable.from(action.payload.events)
          .map(payload => {
            return {
              type: GET_EVENTS_SUCCESS,
              payload : action.payload.events
            };
        }),
      Rx.Observable.fromPromise(api.addFavouriteEvent(action.payload.data))
      .map(payload => {
      
        return {
          type: FAVOURITE_EVENT_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: FAVOURITE_EVENT_ERROR,
          payload: { error }
        }) 
      )
      .takeUntil(action$.ofType(FAVOURITE_EVENT_CANCELLED))
    ));


  //* Remove Favoutite *//
  
  /*
    Action creators
  */
  export const removeFavouriteEvent = payload => ({
    type: REMOVE_FAVOURITE_EVENT,
    payload,
  });
  
  /*
    Epics
  */
  /* eslint-disable */
  export const removeFavouriteEventEpic = action$ =>
  action$.ofType(REMOVE_FAVOURITE_EVENT)
    .flatMap(action =>
      Rx.Observable.concat(
        Rx.Observable.from(action.payload.events)
          .map(payload => {
            return {
              type: GET_EVENTS_SUCCESS,
              payload : action.payload.events
            };
        }),
        Rx.Observable.fromPromise(api.removeFavouriteEvent(action.payload.data))
        .map(payload => {
          return {
            type: FAVOURITE_EVENT_SUCCESS,
            payload
          };
        })
        .catch(error =>
          Rx.Observable.of({
            type: FAVOURITE_EVENT_ERROR,
            payload: { error }
          }) 
        )
      )
    );

    export const addFavouriteEventEpics = combineEpics(addFavouriteEventEpic);
    export const removeFavouriteEventEpics = combineEpics(removeFavouriteEventEpic);

  /* Places Favourite */
  const initialPlaceState = {
    phase: INIT,
    errors: null,
    place: [],
  };
  
  /*
    Reducer
  */
  export function favouritePlaceStore(state = initialPlaceState, action = {}) {
    switch (action.type) {
      case ADD_FAVOURITE_PLACE:
        return {
          ...state,
          phase: LOADING,
        };
      case REMOVE_FAVOURITE_PLACE:
        return {
          ...state,
          phase: LOADING,
        };
      case FAVOURITE_PLACE_SUCCESS:
        return {
          ...state,
          phase: SUCCESS,
          errors: null,
          place: action.payload,
        };
      case FAVOURITE_PLACE_ERROR:
        return {
          ...state,
          phase: ERROR,
          errors: action.payload.error,
        };
      case FAVOURITE_PLACE_CANCELLED:
        return {
          ...state,
          phase: INIT,
          errors: null,
        };
  
      default: {
        return state;
      }
    }
  }
  
  /*
    Action creators
  */
  export const addFavouritePlace = payload => ({
    type: ADD_FAVOURITE_PLACE,
    payload,
  });
  
  /*
    Epics
  */
  /* eslint-disable */
export const addFavouritePlaceEpic = action$ =>
    action$.ofType(ADD_FAVOURITE_PLACE).flatMap(action => 
      Rx.Observable.concat(
        Rx.Observable.from(action.payload.places)
          .map(payload => {
            return {
              type: GET_PLACES_SUCCESS,
              payload : action.payload.places
            };
        }),
        Rx.Observable.fromPromise(api.addFavouritePlace(action.payload.data))
        .map(payload => {    
          return {
            type: FAVOURITE_PLACE_SUCCESS,
            payload
          };
        })
        .catch(error =>
          Rx.Observable.of({
            type: FAVOURITE_PLACE_ERROR,
            payload: { error }
          }) 
        )
        .takeUntil(action$.ofType(FAVOURITE_PLACE_CANCELLED))
      ));
  
  
    //* Remove Favoutite *//
    
    /*
      Action creators
    */
    export const removeFavouritePlace = payload => ({
      type: REMOVE_FAVOURITE_PLACE,
      payload,
    });
    
    /*
      Epics
    */
    /* eslint-disable */
    export const removeFavouritePlaceEpic = action$ =>
    action$.ofType(REMOVE_FAVOURITE_PLACE)
      .flatMap(action =>
        Rx.Observable.concat(
          Rx.Observable.from(action.payload.places)
          .map(payload => {
            return {
              type: GET_PLACES_SUCCESS,
              payload : action.payload.places
            };
          }),
          Rx.Observable.fromPromise(api.removeFavouritePlace(action.payload.data))
          .map(payload => {
            return {
              type: FAVOURITE_PLACE_SUCCESS,
              payload
            };
          })
          .catch(error =>
            Rx.Observable.of({
              type: FAVOURITE_PLACE_ERROR,
              payload: { error }
            }) 
          )
        )
      );

export const addFavouritePlaceEpics = combineEpics(addFavouritePlaceEpic);
export const removeFavouritePlaceEpics = combineEpics(removeFavouritePlaceEpic);


/* GET FAVOURITE EVENTS */
const favouriteEventInitialState = {
  phase: INIT,
  errors: null,
  events: [],
};

/*
  Reducer
*/
export function favouriteEventsStore(state = favouriteEventInitialState, action = {}) {
  switch (action.type) {
    case GET_FAVOURITE_EVENT:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_FAVOURITE_EVENT_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        events: action.payload,
      };
    case GET_FAVOURITE_EVENT_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_FAVOURITE_EVENT_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getFavouriteEvents = () => ({
  type: GET_FAVOURITE_EVENT
});

/*
  Epics
*/
/* eslint-disable */
export const getFavouriteEventsEpic = action$ =>
  action$.ofType(GET_FAVOURITE_EVENT).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getFavouriteEvents(action.payload))
      .map(payload => {
        return {
          type: GET_FAVOURITE_EVENT_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_FAVOURITE_EVENT_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_FAVOURITE_EVENT_CANCELLED));
  });

  /* GET FAVOURITE PLACES */
const favouritePlaceInitialState = {
  phase: INIT,
  errors: null,
  places: [],
};

/*
  Reducer
*/
export function favouritePlacesStore(state = favouritePlaceInitialState, action = {}) {
  switch (action.type) {
    case GET_FAVOURITE_PLACE:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_FAVOURITE_PLACE_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        places: action.payload,
      };
    case GET_FAVOURITE_PLACE_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_FAVOURITE_PLACE_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getFavouritePlaces = () => ({
  type: GET_FAVOURITE_PLACE
});

/*
  Epics
*/
/* eslint-disable */
export const getFavouritePlacesEpic = action$ =>
  action$.ofType(GET_FAVOURITE_PLACE).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getFavouritePlaces(action.payload))
      .map(payload => {
        return {
          type: GET_FAVOURITE_PLACE_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_FAVOURITE_PLACE_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_FAVOURITE_PLACE_CANCELLED));
  });

  export const getFavouriteEventsEpics = combineEpics(getFavouriteEventsEpic);
  export const getFavouritePlacesEpics = combineEpics(getFavouritePlacesEpic);

 