import { AsyncStorage } from 'react-native';
import fetch from '../../utils/fetch';
import { API_URL } from '../../utils/constants';
import { LOGIN_USER } from '../auth/duck';

export const addFavouriteEvent = payload => fetch(`${API_URL}UserPlannedEvents`, {
  method: 'POST',
  body: JSON.stringify(payload),
}).then(res => res.json());

export const removeFavouriteEvent = payload => fetch(`${API_URL}UserPlannedEvents`, {
  method: 'DELETE',
  body: JSON.stringify(payload),
}).then(res => res.json());

export const addFavouritePlace = payload => fetch(`${API_URL}UserFavoritePlaces`, {
  method: 'POST',
  body: JSON.stringify(payload),
}).then(res => res.json());

export const removeFavouritePlace = payload => fetch(`${API_URL}UserFavoritePlaces`, {
  method: 'DELETE',
  body: JSON.stringify(payload),
}).then(res => res.json());

export const getFavouriteEvents = async () => {
  const { id } = JSON.parse(await AsyncStorage.getItem(LOGIN_USER) || '{}');
  return (
    fetch(`${API_URL}UserPlannedEvents/GetUserPlannedEvents/${id}`, {
      method: 'GET',
    }).then(res => res.json()));
};

export const getFavouritePlaces = async () => {
  const { id } = JSON.parse(await AsyncStorage.getItem(LOGIN_USER) || '{}');
  return (
    fetch(`${API_URL}UserFavoritePlaces/GetUserFavoritePlaces/${id}`, {
      method: 'GET',
    }).then(res => res.json()));
};
