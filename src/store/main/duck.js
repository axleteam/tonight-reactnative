
import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';
// THIS IS AN EXAMAPLE OF LOGIN FUNCTIONALITY
// REDUCER SHOULD BE CALLED LOGIN

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods'; // TODO create file and write function to do async call

/*
  Action types
*/
export const LOGIN_USER = 'tonight/login/LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'tonight/login/LOGIN_USER_SUCCESS';
export const LOGIN_USER_ERROR = 'tonight/login/LOGIN_USER_ERROR';
export const LOGIN_USER_CANCELLED = 'tonight/login/LOGIN_USER_CANCELLED';

/*
  Initial state
*/
const initialState = {
  phase: INIT,
  error: null,
};

/*
  Reducer
*/
export function mainStore(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...state,
        phase: LOADING,
      };
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        error: null,
      };
    case LOGIN_USER_ERROR:
      return {
        ...state,
        phase: ERROR,
        error: action.payload,
      };
    case LOGIN_USER_CANCELLED:
      return {
        ...state,
        phase: INIT,
        error: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const loginUser = (credentials) => {
  return {
    type: LOGIN_USER,
    payload: credentials,
  };
};

/*
  Epics
*/
/* eslint-disable */
export const loginUserEpic = (action$) =>
  action$
    .ofType(LOGIN_USER)
    .mergeMap((action) => {
      return Rx.Observable.fromPromise(api.loginUser(action.payload))
        .flatMap((payload) => ({
          type: LOGIN_USER_SUCCESS,
          payload
        }))
        .catch((error) => Rx.Observable.of({
          type: LOGIN_USER_ERROR,
          payload: { error }
        }))
        .takeUntil(action$.ofType(LOGIN_USER_CANCELLED))
    })

/*
  Export Epics
*/
export const mainEpic = combineEpics(
  loginUserEpic,
);
