import fetch from '../../utils/fetch';
import { API_URL } from '../../utils/constants';

export const addSupport = question => (
  fetch(`${API_URL}Help/AskQuestion`, {
    method: 'POST',
    body: JSON.stringify(question),
  }).then(res => res.json()));
