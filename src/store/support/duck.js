import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';

export const ADD_SUPPORT = 'tonight/support/ADD_SUPPORT';
export const ADD_SUPPORT_SUCCESS = 'tonight/support/ADD_SUPPORT_SUCCESS';
export const ADD_SUPPORT_ERROR = 'tonight/support/ADD_SUPPORT_ERROR';
export const ADD_SUPPORT_CANCELLED = 'tonight/support/ADD_SUPPORT_CANCELLED';
export const CLEAR_SUPPORT = 'tonight/support/CLEAR_SUPPORT';

const initialState = {
  phase: INIT,
  errors: null,
  message: ' ',
};

/*
  Reducer
*/
export function supportStore(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_SUPPORT:
      return {
        ...state,
        phase: LOADING,
      };
    case ADD_SUPPORT_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        message: action.payload,
      };
    case ADD_SUPPORT_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case ADD_SUPPORT_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };
    case CLEAR_SUPPORT:
      return {
        ...initialState,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const addSupportMessage = payload => ({
  type: ADD_SUPPORT,
  payload,
});

/*
  Epics
*/
/* eslint-disable */
export const addSupportEpic = action$ =>
  action$.ofType(ADD_SUPPORT)
    .flatMap(action =>
      Rx.Observable.concat(
        // Fire 2 actions, one after the other
        Rx.Observable.fromPromise(api.addSupport(action.payload))
      .map(payload => {
        return {
          type: ADD_SUPPORT_SUCCESS,
          payload
        };
      }),
      Rx.Observable.of(action).delay(2000).map(()=>{
        return {
          type: CLEAR_SUPPORT,
        };
      })
      ).catch(error =>
        Rx.Observable.of({
          type: ADD_SUPPORT_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(ADD_SUPPORT_CANCELLED))
    );


/*
      Export Epics
    */
export const supportEpic = combineEpics(addSupportEpic);
