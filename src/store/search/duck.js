import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';

export const GET_SEARCH_EVENTS = 'tonight/search/GET_SEARCH_EVENTS';
export const GET_SEARCH_EVENTS_SUCCESS = 'tonight/search/GET_SEARCH_EVENTS_SUCCESS';
export const GET_SEARCH_EVENTS_ERROR = 'tonight/search/GET_SEARCH_EVENTS_ERROR';
export const GET_SEARCH_EVENTS_CANCELLED = 'tonight/search/GET_SEARCH_EVENTS_CANCELLED';
export const CLEAR_SEARCH_EVENTS = 'tonight/search/CLEAR_SEARCH_EVENTS';

const initialState = {
  phase: INIT,
  errors: null,
  searches: {
    places: [],
    events: [],
  },
};

/*
  Reducer
*/
export function searchStore(state = initialState, action = {}) {
  switch (action.type) {
    case GET_SEARCH_EVENTS:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_SEARCH_EVENTS_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        searches: action.payload,
      };
    case GET_SEARCH_EVENTS_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_SEARCH_EVENTS_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };
    case CLEAR_SEARCH_EVENTS:
      return {
        ...initialState,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const searchEvents = payload => ({
  type: GET_SEARCH_EVENTS,
  payload,
});

/*
  Epics
*/
/* eslint-disable */
export const getSearchEpic = action$ =>
  action$.ofType(GET_SEARCH_EVENTS).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getSearchEvents(action.payload))
      .map(payload => {
        return {
          type: GET_SEARCH_EVENTS_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_SEARCH_EVENTS_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_SEARCH_EVENTS_CANCELLED));
  });

  export const clearSearch = () => ({
    type: CLEAR_SEARCH_EVENTS
  });

/*
      Export Epics
    */
export const searchEpic = combineEpics(getSearchEpic);