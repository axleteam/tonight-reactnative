import fetch from '../../utils/fetch';
import { API_URL } from '../../utils/constants';

export const getSearchEvents = text => (
  fetch(`${API_URL}Search/GetByName/${text}`, {
    method: 'GET',
  }).then(res => res.json()));
