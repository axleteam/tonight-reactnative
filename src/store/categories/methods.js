import fetch from '../../utils/fetch';
import { API_URL } from '../../utils/constants';

export const getCategory = () => fetch(`${API_URL}EventSubcategories`).then(res => res.json());
