import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';

export const GET_CATEGORY = 'tonight/login/GET_CATEGORY';
export const GET_CATEGORY_SUCCESS = 'tonight/login/GET_CATEGORY_SUCCESS';
export const GET_CATEGORY_ERROR = 'tonight/login/GET_CATEGORY_ERROR';
export const GET_CATEGORY_CANCELLED = 'tonight/login/GET_CATEGORY_CANCELLED';


const initialState = {
  phase: INIT,
  errors: null,
  categories: [],
};

/*
  Reducer
*/
export function categoryStore(state = initialState, action = {}) {
  switch (action.type) {
    case GET_CATEGORY:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_CATEGORY_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        categories: action.payload,
      };
    case GET_CATEGORY_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_CATEGORY_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getCategory = () => ({
  type: GET_CATEGORY,
});

/*
  Epics
*/
/* eslint-disable */
export const getCategoryEpic = action$ =>
  action$.ofType(GET_CATEGORY).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getCategory())
      .map(payload => {
        console.log(payload);
        return {
          type: GET_CATEGORY_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_CATEGORY_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_CATEGORY_CANCELLED));
  });

/*
      Export Epics
    */
export const categoryEpic = combineEpics(getCategoryEpic);
