import { combineEpics } from 'redux-observable';
import { combineReducers } from 'redux';

// import { mainStore, mainEpic } from "./main/duck";
import { loginStore, loginEpic, registerEpic, registerStore, forgotStore, forgotEpic } from './auth/duck';
import {
  eventsEpic,
  eventsStore,
  eventDetailEpic,
  eventDetailStore,
  eventsRegionEpic,
  eventsRegionStore,
} from './event/duck';
import { categoryEpic, categoryStore } from './categories/duck';
import {
  favouriteEventStore,
  addFavouriteEventEpics,
  removeFavouriteEventEpics,
  favouritePlaceStore,
  addFavouritePlaceEpics,
  removeFavouritePlaceEpics,
  favouriteEventsStore,
  getFavouriteEventsEpics,
  favouritePlacesStore,
  getFavouritePlacesEpics,
} from './favourite/duck';
import { placesEpic, placesStore, placeDetailEpic, placeDetailStore } from './places/duck';
import { tagsEpic, tagsStore } from './tags/duck';
import { searchEpic, searchStore } from './search/duck';
import { supportEpic, supportStore } from './support/duck';
import { profileStore, profileUserEpic, updateProfileEpic } from './profile/duck';

export const rootEpic = combineEpics(
  loginEpic,
  registerEpic,
  forgotEpic,
  eventsEpic,
  categoryEpic,
  addFavouriteEventEpics,
  removeFavouriteEventEpics,
  addFavouritePlaceEpics,
  removeFavouritePlaceEpics,
  getFavouriteEventsEpics,
  getFavouritePlacesEpics,
  placesEpic,
  eventDetailEpic,
  placeDetailEpic,
  tagsEpic,
  eventsRegionEpic,
  searchEpic,
  supportEpic,
  profileUserEpic,
  updateProfileEpic,
);

export const rootReducer = combineReducers({
  loginStore,
  registerStore,
  forgotStore,
  eventsStore,
  categoryStore,
  favouriteEventStore,
  favouritePlaceStore,
  favouriteEventsStore,
  favouritePlacesStore,
  placesStore,
  eventDetailStore,
  placeDetailStore,
  tagsStore,
  eventsRegionStore,
  searchStore,
  supportStore,
  profileStore,
});
