import fetch from '../../utils/fetch';
import { API_URL } from '../../utils/constants';

export const getPlaces = filters => (
  fetch(`${API_URL}Places/GetPlaceByFilter`, {
    method: 'POST',
    body: JSON.stringify(filters),
  }).then(res => res.json()));

export const getPlaceDetail = id => fetch(`${API_URL}Places/${id}`).then(res => res.json());
