import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';

export const GET_PLACES = 'tonight/login/GET_PLACES';
export const GET_PLACES_SUCCESS = 'tonight/login/GET_PLACES_SUCCESS';
export const GET_PLACES_ERROR = 'tonight/login/GET_PLACES_ERROR';
export const GET_PLACES_CANCELLED = 'tonight/login/GET_PLACES_CANCELLED';

export const GET_PLACE_DETAIL = 'tonight/PLACE/GET_PLACE_DETAIL';
export const GET_PLACE_DETAIL_SUCCESS = 'tonight/PLACE/GET_PLACE_DETAIL_SUCCESS';
export const GET_PLACE_DETAIL_ERROR = 'tonight/PLACE/GET_PLACE_DETAIL_ERROR';
export const GET_PLACE_DETAIL_CANCELLED = 'tonight/PLACE/GET_PLACE_DETAIL_CANCELLED';

const initialState = {
  phase: INIT,
  errors: null,
  places: [],
  filters: {},
};

/*
  Reducer
*/
export function placesStore(state = initialState, action = {}) {
  switch (action.type) {
    case GET_PLACES:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_PLACES_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        places: action.payload,
        filters: action.filters === undefined ? [] : action.filters,
      };
    case GET_PLACES_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_PLACES_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getPlaces = payload => ({
  type: GET_PLACES,
  payload,
});

/*
  Epics
*/
/* eslint-disable */
export const getPlacesEpic = action$ =>
  action$.ofType(GET_PLACES).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getPlaces(action.payload))
      .map(payload => {
        console.log(payload);
        return {
          type: GET_PLACES_SUCCESS,
          filters:action.payload,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_PLACES_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_PLACES_CANCELLED));
  });


/* Place Detail Reducer */
const placeDetailInitialState = {
  phase: INIT,
  errors: null,
  place: [],
};

/*
  Reducer
*/
export function placeDetailStore(state = placeDetailInitialState, action = {}) {
  switch (action.type) {
    case GET_PLACE_DETAIL:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_PLACE_DETAIL_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        place: action.payload,
      };
    case GET_PLACE_DETAIL_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_PLACE_DETAIL_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getPlaceDetail= (payload) => ({
  type: GET_PLACE_DETAIL,
  payload
});

/*
  Epics
*/
/* eslint-disable */
export const getPlaceDetailEpic = action$ =>
  action$.ofType(GET_PLACE_DETAIL).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getPlaceDetail(action.payload))
      .map(payload => {
        console.log(payload);
        return {
          type: GET_PLACE_DETAIL_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_PLACE_DETAIL_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_PLACE_DETAIL_CANCELLED));
  });

/*
      Export Epics
    */
export const placesEpic = combineEpics(getPlacesEpic);
export const placeDetailEpic = combineEpics(getPlaceDetailEpic);