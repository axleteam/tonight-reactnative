import Rx from 'rxjs/Rx';
import { combineEpics } from 'redux-observable';

import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constants';
import * as api from './methods';

export const GET_TAGS = 'tonight/filter/GET_TAGS';
export const GET_TAGS_SUCCESS = 'tonight/filter/GET_TAGS_SUCCESS';
export const GET_TAGS_ERROR = 'tonight/filter/GET_TAGS_ERROR';
export const GET_TAGS_CANCELLED = 'tonight/filter/GET_TAGS_CANCELLED';

const initialState = {
  phase: INIT,
  errors: null,
  tags: [],
};

/*
  Reducer
*/
export function tagsStore(state = initialState, action = {}) {
  switch (action.type) {
    case GET_TAGS:
      return {
        ...state,
        phase: LOADING,
      };
    case GET_TAGS_SUCCESS:
      return {
        ...state,
        phase: SUCCESS,
        errors: null,
        tags: action.payload,
      };
    case GET_TAGS_ERROR:
      return {
        ...state,
        phase: ERROR,
        errors: action.payload.error,
      };
    case GET_TAGS_CANCELLED:
      return {
        ...state,
        phase: INIT,
        errors: null,
      };

    default: {
      return state;
    }
  }
}

/*
  Action creators
*/
export const getTags = () => ({
  type: GET_TAGS,
});

/*
  Epics
*/
/* eslint-disable */
export const getTagsEpic = action$ =>
  action$.ofType(GET_TAGS).mergeMap(action => {
    return Rx.Observable.fromPromise(api.getTags())
      .map(payload => {
        console.log(payload);
        return {
          type: GET_TAGS_SUCCESS,
          payload
        };
      })
      .catch(error =>
        Rx.Observable.of({
          type: GET_TAGS_ERROR,
          payload: { error }
        })
      )
      .takeUntil(action$.ofType(GET_TAGS_CANCELLED));
  });


/*
      Export Epics
    */
export const tagsEpic = combineEpics(getTagsEpic);