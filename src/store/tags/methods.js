import fetch from '../../utils/fetch';
import { API_URL } from '../../utils/constants';

export const getTags = () => fetch(`${API_URL}Tags`).then(res => res.json());
