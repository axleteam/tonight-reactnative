import { StackNavigator } from 'react-navigation';
import HomeScreen from './containers/HomeScreen';
import LoginScreen from './containers/LoginScreen';
import RegisterScreen from './containers/RegisterScreen';
import ForgotScreen from './containers/ForgotScreen';
import EventTabs from './containers/MainScreenTabs';
import EventDetail from './containers/MainScreenTabs/Events/EventDetail';
import PlaceDetail from './containers/MainScreenTabs/Events/PlaceDetail';
import Filter from './containers/Filter';
import Success from './containers/Success';
import Support from './containers/Support';
import EditProfile from './containers/EditProfile';
import NotificationSettings from './containers/NotificationSettings';

const TonightApp = new StackNavigator(
  {
    Home: HomeScreen,
    Login: LoginScreen,
    Register: RegisterScreen,
    ForgotPassword: ForgotScreen,
    Events: EventTabs,
    EventDetail,
    PlaceDetail,
    Filter,
    Success,
    Support,
    EditProfile,
    NotificationSettings,
  },
  {
    initialRouteName: 'Home',
  },
);

export default TonightApp;
