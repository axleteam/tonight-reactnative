import { StyleSheet, Dimensions, StatusBar, Platform } from 'react-native';

const { width } = Dimensions.get('window');
const globalStyles = StyleSheet.create({
  inputStyle: {
    color: '#fff',
  },
  labelStyle: {
    color: '#80282828',
    flex: 1.5,
    paddingLeft: 12,
  },
  bgImageContainer: {
    flex: 1,
    width: '100%',
  },
  itemStyle: {
    height: 50,
    borderBottomWidth: 0,
  },
  inputMargin: {
    marginTop: 10,
  },
  errorStyle: {
    color: 'red',
    marginLeft: 5,
    marginTop: 5,
    fontSize: 16,
  },
  loader: {
    position: 'absolute',
    left: width / 2.1,
    top: width / 1.5,
    zIndex: 99999999999,
  },
  labelView: {
    position: 'absolute',
    right: 10,
    flexDirection: 'row',
    backgroundColor: 'transparent',
  },
  label: {
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    padding: 4,
    marginLeft: 2,
    borderRadius: 30,
  },
  labelColor: {
    color: '#bf178e',
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 11,
  },
  statusBarStyle: {
    height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
  },
  textCenter: {
    textAlign: 'center',
  },
  safeViewStyle: {
    flex: 1,
    backgroundColor: '#0F033E',
    paddingTop: Platform.OS === 'ios' ? 0 : 20,
  },
  headerTitleStyle: {
    textAlign: 'center',
    alignSelf: 'center',
    width: '80%',
  },
  headerStyle: {
    borderBottomWidth: 0,
    paddingTop: StatusBar.currentHeight,
  },
});

export default globalStyles;
